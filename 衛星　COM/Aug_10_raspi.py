
import serial
import binascii
import time

with open("./image_dec.jpg", 'rb') as image:
    f = image.read()


ser = serial.Serial('/dev/ttyS0','115200',timeout=0.5)


flag = 0x7e
to_address = [0x94, 0xa2, 0x62, 0x64, 0x96, 0x98, 0x60]
from_address = [0x94, 0xa2, 0x62, 0x64, 0x96, 0x98, 0x61]
control = 0x03
pid = 0xf0
start_image = b'start'
end_image = b'end'
fcs = 0xffff

image_num = 0x00

max_imge_num = len(f) // 246 + 1

max_imge_num_change = int(str(max_imge_num)[-2::])

count=0

div_f = list()

for n in range(max_imge_num):
    i = 240 * n
    div_f.append(f[i:i+240])
    


##CRC Calc
crc_Reg = 0x0001
calc = 0x8404

data_num = 30

CRC = b'CRC'

CRC_data = []

for x in range(max_imge_num):
    
    crc_Reg = 0x0001
    calc = 0x8404
    
    data = div_f[x]
    cal_data = list()

    for i in range(len(data)):
        cal_data.append(data[i])

        for n in range(8):
            w = (crc_Reg ^ cal_data[i]) & 0x0001
            crc_Reg = crc_Reg >> 1

            if w == 1:
                crc_Reg = crc_Reg ^ calc

            cal_data[i] = cal_data[i] >> 1


    crc_Reg = crc_Reg ^ 0xFFFF
    crc_Reg2 = crc_Reg >> 8

    
    CRC_data.append(CRC + crc_Reg.to_bytes(2, 'big') + crc_Reg2.to_bytes(1, 'big'))



for n in range(max_imge_num):
    packet = b""
    packet += flag.to_bytes(1, 'big')

    for i in range(len(to_address)):
        packet += to_address[i].to_bytes(1, 'big')

    for i in range(len(from_address)):
        packet += from_address[i].to_bytes(1, 'big')

    packet += control.to_bytes(1, 'big')
    packet += pid.to_bytes(1, 'big')
    packet += CRC_data[n]
    packet += start_image
    packet += max_imge_num_change.to_bytes(1, 'big')
    packet += image_num.to_bytes(1, 'big')
    packet += div_f[n]
    packet += end_image
    packet += fcs.to_bytes(2, 'big')
    packet += flag.to_bytes(1, 'big')
    image_num += 1
    
    if(image_num >= 100):
        image_num = 0

    ser.write(packet)
    time.sleep(0.1)
    

ser.close()


