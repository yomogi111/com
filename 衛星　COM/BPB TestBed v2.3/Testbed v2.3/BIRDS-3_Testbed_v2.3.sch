<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="9" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="23" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="63" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="12" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CN30x _EGSE">
<packages>
<package name="SHDR50W60P200_2X25_5000X400X44">
<description>&lt;b&gt;A3C-50DA-2DSA(71)&lt;/b&gt;&lt;br&gt;
</description>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.3"/>
<pad name="4" x="2" y="0" drill="0.7" diameter="1.3"/>
<pad name="6" x="4" y="0" drill="0.7" diameter="1.3"/>
<pad name="8" x="6" y="0" drill="0.7" diameter="1.3"/>
<pad name="10" x="8" y="0" drill="0.7" diameter="1.3"/>
<pad name="12" x="10" y="0" drill="0.7" diameter="1.3"/>
<pad name="14" x="12" y="0" drill="0.7" diameter="1.3"/>
<pad name="16" x="14" y="0" drill="0.7" diameter="1.3"/>
<pad name="18" x="16" y="0" drill="0.7" diameter="1.3"/>
<pad name="20" x="18" y="0" drill="0.7" diameter="1.3"/>
<pad name="22" x="20" y="0" drill="0.7" diameter="1.3"/>
<pad name="24" x="22" y="0" drill="0.7" diameter="1.3"/>
<pad name="26" x="24" y="0" drill="0.7" diameter="1.3"/>
<pad name="28" x="26" y="0" drill="0.7" diameter="1.3"/>
<pad name="30" x="28" y="0" drill="0.7" diameter="1.3"/>
<pad name="32" x="30" y="0" drill="0.7" diameter="1.3"/>
<pad name="34" x="32" y="0" drill="0.7" diameter="1.3"/>
<pad name="36" x="34" y="0" drill="0.7" diameter="1.3"/>
<pad name="38" x="36" y="0" drill="0.7" diameter="1.3"/>
<pad name="40" x="38" y="0" drill="0.7" diameter="1.3"/>
<pad name="42" x="40" y="0" drill="0.7" diameter="1.3"/>
<pad name="44" x="42" y="0" drill="0.7" diameter="1.3"/>
<pad name="46" x="44" y="0" drill="0.7" diameter="1.3"/>
<pad name="48" x="46" y="0" drill="0.7" diameter="1.3"/>
<pad name="50" x="48" y="0" drill="0.7" diameter="1.3"/>
<pad name="1" x="0" y="2" drill="0.7" diameter="1.3" shape="square"/>
<pad name="3" x="2" y="2" drill="0.7" diameter="1.3"/>
<pad name="5" x="4" y="2" drill="0.7" diameter="1.3"/>
<pad name="7" x="6" y="2" drill="0.7" diameter="1.3"/>
<pad name="9" x="8" y="2" drill="0.7" diameter="1.3"/>
<pad name="11" x="10" y="2" drill="0.7" diameter="1.3"/>
<pad name="13" x="12" y="2" drill="0.7" diameter="1.3"/>
<pad name="15" x="14" y="2" drill="0.7" diameter="1.3"/>
<pad name="17" x="16" y="2" drill="0.7" diameter="1.3"/>
<pad name="19" x="18" y="2" drill="0.7" diameter="1.3"/>
<pad name="21" x="20" y="2" drill="0.7" diameter="1.3"/>
<pad name="23" x="22" y="2" drill="0.7" diameter="1.3"/>
<pad name="25" x="24" y="2" drill="0.7" diameter="1.3"/>
<pad name="27" x="26" y="2" drill="0.7" diameter="1.3"/>
<pad name="29" x="28" y="2" drill="0.7" diameter="1.3"/>
<pad name="31" x="30" y="2" drill="0.7" diameter="1.3"/>
<pad name="33" x="32" y="2" drill="0.7" diameter="1.3"/>
<pad name="35" x="34" y="2" drill="0.7" diameter="1.3"/>
<pad name="37" x="36" y="2" drill="0.7" diameter="1.3"/>
<pad name="39" x="38" y="2" drill="0.7" diameter="1.3"/>
<pad name="41" x="40" y="2" drill="0.7" diameter="1.3"/>
<pad name="43" x="42" y="2" drill="0.7" diameter="1.3"/>
<pad name="45" x="44" y="2" drill="0.7" diameter="1.3"/>
<pad name="47" x="46" y="2" drill="0.7" diameter="1.3"/>
<pad name="49" x="48" y="2" drill="0.7" diameter="1.3"/>
<text x="-3" y="1" size="1.27" layer="25" rot="R90" align="center">&gt;NAME</text>
<wire x1="-1.4" y1="-1.4" x2="-1.4" y2="3.4" width="0.15" layer="21"/>
<wire x1="-1.4" y1="3.4" x2="49.4" y2="3.4" width="0.15" layer="21"/>
<wire x1="49.4" y1="3.4" x2="49.4" y2="-1.4" width="0.15" layer="21"/>
<wire x1="49.4" y1="-1.4" x2="-1.4" y2="-1.4" width="0.15" layer="21"/>
<wire x1="0.889" y1="1.143" x2="0.889" y2="2.921" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.921" x2="-0.889" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.921" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.921" x2="0.889" y2="3.048" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.175" x2="0.889" y2="3.556" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.683" x2="0.889" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0.889" y1="4.191" x2="0.889" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.889" y1="4.572" x2="0.889" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.889" y1="4.699" x2="-0.889" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="4.699" x2="-0.889" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="3.175" x2="-0.889" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="3.048" x2="-0.889" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="3.048" x2="0.889" y2="3.048" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.048" x2="0.889" y2="3.175" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.175" x2="-0.889" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="3.175" x2="-0.889" y2="3.302" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="3.302" x2="0.762" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.302" x2="0.762" y2="3.429" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.429" x2="-0.762" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.429" x2="-0.762" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.556" x2="0.889" y2="3.556" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.556" x2="0.889" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.683" x2="-0.762" y2="3.683" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.683" x2="-0.762" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.683" x2="-0.762" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.81" x2="0.762" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.81" x2="0.762" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.937" x2="-0.762" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.937" x2="-0.762" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.064" x2="0.762" y2="4.064" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.064" x2="0.762" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.937" x2="0.889" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0.889" y1="3.937" x2="0.889" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0.889" y1="4.191" x2="-0.762" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.191" x2="-0.762" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.318" x2="0.762" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.318" x2="0.762" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.445" x2="-0.762" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.445" x2="-0.762" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.572" x2="0.889" y2="4.572" width="0.1524" layer="21"/>
<text x="0.5" y="-3" size="1.27" layer="21" rot="R90">2</text>
<text x="8.5" y="-4" size="1.27" layer="21" rot="R90">10</text>
<text x="18.5" y="-4" size="1.27" layer="21" rot="R90">20</text>
<text x="28.5" y="-4" size="1.27" layer="21" rot="R90">30</text>
<text x="38.5" y="-4" size="1.27" layer="21" rot="R90">40</text>
<text x="48.5" y="-4" size="1.27" layer="21" rot="R90">50</text>
<text x="4.5" y="4.5" size="1.27" layer="21" rot="R90">5</text>
<text x="14.5" y="4.5" size="1.27" layer="21" rot="R90">15</text>
<text x="24.5" y="4.5" size="1.27" layer="21" rot="R90">25</text>
<text x="34.5" y="4.5" size="1.27" layer="21" rot="R90">35</text>
<text x="44.5" y="4.5" size="1.27" layer="21" rot="R90">45</text>
</package>
<package name="POWER_PROBE">
<wire x1="-1.016" y1="0" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="0" x2="-1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-2.54" x2="1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.27" y1="2.286" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.286" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-1.27" drill="0.9144" shape="long"/>
<pad name="2" x="0" y="1.27" drill="0.9144" shape="long"/>
<text x="-2.151" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.3048" y1="0.9652" x2="0.3048" y2="1.5748" layer="51"/>
<rectangle x1="-0.3048" y1="-1.5748" x2="0.3048" y2="-0.9652" layer="51"/>
</package>
<package name="2X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<text x="-2.54" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<pad name="6" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
</package>
<package name="2X06_REV">
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="8.89" y2="1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.97" y2="1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="7.62" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="7.62" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="10.16" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="10.16" y="1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="12.7" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="12.7" y="1.27" drill="1.016" shape="octagon"/>
<text x="-1.27" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="7.366" y1="1.016" x2="7.874" y2="1.524" layer="51"/>
<rectangle x1="9.906" y1="1.016" x2="10.414" y2="1.524" layer="51"/>
<rectangle x1="7.366" y1="-1.524" x2="7.874" y2="-1.016" layer="51"/>
<rectangle x1="9.906" y1="-1.524" x2="10.414" y2="-1.016" layer="51"/>
<rectangle x1="12.446" y1="1.016" x2="12.954" y2="1.524" layer="51"/>
<rectangle x1="12.446" y1="-1.524" x2="12.954" y2="-1.016" layer="51"/>
</package>
<package name="2X10_REV">
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="8.89" y2="1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.97" y2="1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="7.62" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="7.62" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="10.16" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="10.16" y="1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="12.7" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="12.7" y="1.27" drill="1.016" shape="octagon"/>
<text x="-1.27" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="7.366" y1="1.016" x2="7.874" y2="1.524" layer="51"/>
<rectangle x1="9.906" y1="1.016" x2="10.414" y2="1.524" layer="51"/>
<rectangle x1="7.366" y1="-1.524" x2="7.874" y2="-1.016" layer="51"/>
<rectangle x1="9.906" y1="-1.524" x2="10.414" y2="-1.016" layer="51"/>
<rectangle x1="12.446" y1="1.016" x2="12.954" y2="1.524" layer="51"/>
<rectangle x1="12.446" y1="-1.524" x2="12.954" y2="-1.016" layer="51"/>
<wire x1="13.97" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="2.54" x2="16.51" y2="1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<pad name="14" x="15.24" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="15.24" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="14.986" y1="1.016" x2="15.494" y2="1.524" layer="51"/>
<rectangle x1="14.986" y1="-1.524" x2="15.494" y2="-1.016" layer="51"/>
<wire x1="16.51" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-2.54" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="2.54" x2="19.05" y2="1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.05" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="-2.54" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="2.54" x2="21.59" y2="1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<pad name="16" x="17.78" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="17.78" y="1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="20.32" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="20.32" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="17.526" y1="1.016" x2="18.034" y2="1.524" layer="51"/>
<rectangle x1="17.526" y1="-1.524" x2="18.034" y2="-1.016" layer="51"/>
<rectangle x1="20.066" y1="1.016" x2="20.574" y2="1.524" layer="51"/>
<rectangle x1="20.066" y1="-1.524" x2="20.574" y2="-1.016" layer="51"/>
<wire x1="21.59" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="-2.54" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="2.54" x2="24.13" y2="1.905" width="0.1524" layer="21"/>
<wire x1="24.13" y1="1.905" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<pad name="20" x="22.86" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="22.86" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="22.606" y1="1.016" x2="23.114" y2="1.524" layer="51"/>
<rectangle x1="22.606" y1="-1.524" x2="23.114" y2="-1.016" layer="51"/>
</package>
<package name="2X11_REV">
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="8.89" y2="1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.97" y2="1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="7.62" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="7.62" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="10.16" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="10.16" y="1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="12.7" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="12.7" y="1.27" drill="1.016" shape="octagon"/>
<text x="-1.27" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="7.366" y1="1.016" x2="7.874" y2="1.524" layer="51"/>
<rectangle x1="9.906" y1="1.016" x2="10.414" y2="1.524" layer="51"/>
<rectangle x1="7.366" y1="-1.524" x2="7.874" y2="-1.016" layer="51"/>
<rectangle x1="9.906" y1="-1.524" x2="10.414" y2="-1.016" layer="51"/>
<rectangle x1="12.446" y1="1.016" x2="12.954" y2="1.524" layer="51"/>
<rectangle x1="12.446" y1="-1.524" x2="12.954" y2="-1.016" layer="51"/>
<wire x1="13.97" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="2.54" x2="16.51" y2="1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<pad name="14" x="15.24" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="15.24" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="14.986" y1="1.016" x2="15.494" y2="1.524" layer="51"/>
<rectangle x1="14.986" y1="-1.524" x2="15.494" y2="-1.016" layer="51"/>
<wire x1="16.51" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-2.54" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="2.54" x2="19.05" y2="1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.05" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="-2.54" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="2.54" x2="21.59" y2="1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<pad name="16" x="17.78" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="17.78" y="1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="20.32" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="20.32" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="17.526" y1="1.016" x2="18.034" y2="1.524" layer="51"/>
<rectangle x1="17.526" y1="-1.524" x2="18.034" y2="-1.016" layer="51"/>
<rectangle x1="20.066" y1="1.016" x2="20.574" y2="1.524" layer="51"/>
<rectangle x1="20.066" y1="-1.524" x2="20.574" y2="-1.016" layer="51"/>
<wire x1="21.59" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="-2.54" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="2.54" x2="24.13" y2="1.905" width="0.1524" layer="21"/>
<wire x1="24.13" y1="1.905" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<pad name="20" x="22.86" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="22.86" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="22.606" y1="1.016" x2="23.114" y2="1.524" layer="51"/>
<rectangle x1="22.606" y1="-1.524" x2="23.114" y2="-1.016" layer="51"/>
<wire x1="24.13" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="26.035" y1="-2.54" x2="26.67" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="24.13" y1="1.905" x2="24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="26.035" y2="2.54" width="0.1524" layer="21"/>
<wire x1="26.035" y1="2.54" x2="26.67" y2="1.905" width="0.1524" layer="21"/>
<wire x1="26.67" y1="1.905" x2="26.67" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="26.035" y2="-2.54" width="0.1524" layer="21"/>
<pad name="22" x="25.4" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="25.4" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="25.146" y1="1.016" x2="25.654" y2="1.524" layer="51"/>
<rectangle x1="25.146" y1="-1.524" x2="25.654" y2="-1.016" layer="51"/>
</package>
<package name="2X13_REV">
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="8.89" y2="1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="11.43" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="1.905" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="11.43" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="1.905" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.97" y2="1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="13.97" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<pad name="2" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="7.62" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="7.62" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="10.16" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="10.16" y="1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="12.7" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="12.7" y="1.27" drill="1.016" shape="octagon"/>
<text x="-1.27" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="7.366" y1="1.016" x2="7.874" y2="1.524" layer="51"/>
<rectangle x1="9.906" y1="1.016" x2="10.414" y2="1.524" layer="51"/>
<rectangle x1="7.366" y1="-1.524" x2="7.874" y2="-1.016" layer="51"/>
<rectangle x1="9.906" y1="-1.524" x2="10.414" y2="-1.016" layer="51"/>
<rectangle x1="12.446" y1="1.016" x2="12.954" y2="1.524" layer="51"/>
<rectangle x1="12.446" y1="-1.524" x2="12.954" y2="-1.016" layer="51"/>
<wire x1="13.97" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="13.97" y1="1.905" x2="14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="2.54" x2="16.51" y2="1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="16.51" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="14.605" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<pad name="14" x="15.24" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="15.24" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="14.986" y1="1.016" x2="15.494" y2="1.524" layer="51"/>
<rectangle x1="14.986" y1="-1.524" x2="15.494" y2="-1.016" layer="51"/>
<wire x1="16.51" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="-2.54" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="16.51" y1="1.905" x2="17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="18.415" y1="2.54" x2="19.05" y2="1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.05" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.05" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="-2.54" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.05" y1="1.905" x2="19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="20.955" y1="2.54" x2="21.59" y2="1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="21.59" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<pad name="16" x="17.78" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="17.78" y="1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="20.32" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="20.32" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="17.526" y1="1.016" x2="18.034" y2="1.524" layer="51"/>
<rectangle x1="17.526" y1="-1.524" x2="18.034" y2="-1.016" layer="51"/>
<rectangle x1="20.066" y1="1.016" x2="20.574" y2="1.524" layer="51"/>
<rectangle x1="20.066" y1="-1.524" x2="20.574" y2="-1.016" layer="51"/>
<wire x1="21.59" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="-2.54" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="21.59" y1="1.905" x2="22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="23.495" y1="2.54" x2="24.13" y2="1.905" width="0.1524" layer="21"/>
<wire x1="24.13" y1="1.905" x2="24.13" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<pad name="20" x="22.86" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="22.86" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="22.606" y1="1.016" x2="23.114" y2="1.524" layer="51"/>
<rectangle x1="22.606" y1="-1.524" x2="23.114" y2="-1.016" layer="51"/>
<wire x1="24.13" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="26.035" y1="-2.54" x2="26.67" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="24.13" y1="1.905" x2="24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="26.035" y2="2.54" width="0.1524" layer="21"/>
<wire x1="26.035" y1="2.54" x2="26.67" y2="1.905" width="0.1524" layer="21"/>
<wire x1="26.67" y1="1.905" x2="26.67" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="26.035" y2="-2.54" width="0.1524" layer="21"/>
<pad name="22" x="25.4" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="25.4" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="25.146" y1="1.016" x2="25.654" y2="1.524" layer="51"/>
<rectangle x1="25.146" y1="-1.524" x2="25.654" y2="-1.016" layer="51"/>
<pad name="23" x="27.94" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="27.686" y1="1.016" x2="28.194" y2="1.524" layer="51"/>
<pad name="24" x="27.94" y="-1.27" drill="1.016" shape="octagon"/>
<rectangle x1="27.686" y1="-1.524" x2="28.194" y2="-1.016" layer="51"/>
<pad name="25" x="30.48" y="1.27" drill="1.016" shape="octagon"/>
<rectangle x1="30.226" y1="1.016" x2="30.734" y2="1.524" layer="51"/>
<pad name="26" x="30.48" y="-1.27" drill="1.016" shape="octagon"/>
<rectangle x1="30.226" y1="-1.524" x2="30.734" y2="-1.016" layer="51"/>
<wire x1="26.67" y1="-1.905" x2="27.305" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="28.575" y1="-2.54" x2="29.21" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="26.67" y1="1.905" x2="27.305" y2="2.54" width="0.1524" layer="21"/>
<wire x1="27.305" y1="2.54" x2="28.575" y2="2.54" width="0.1524" layer="21"/>
<wire x1="28.575" y1="2.54" x2="29.21" y2="1.905" width="0.1524" layer="21"/>
<wire x1="29.21" y1="1.905" x2="29.21" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="27.305" y1="-2.54" x2="28.575" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="29.21" y1="-1.905" x2="29.845" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="31.115" y1="-2.54" x2="31.75" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="29.21" y1="1.905" x2="29.845" y2="2.54" width="0.1524" layer="21"/>
<wire x1="29.845" y1="2.54" x2="31.115" y2="2.54" width="0.1524" layer="21"/>
<wire x1="31.115" y1="2.54" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="31.75" y1="1.905" x2="31.75" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="29.845" y1="-2.54" x2="31.115" y2="-2.54" width="0.1524" layer="21"/>
</package>
<package name="3,2-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="7347066">
<pad name="2" x="0" y="0" drill="2.03" shape="long" rot="R180"/>
<pad name="1" x="0" y="4.7" drill="2.03" shape="long" rot="R180"/>
<pad name="3" x="0" y="-4.7" drill="2.03" shape="long" rot="R180"/>
<wire x1="-3.43" y1="6.4" x2="-3.43" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="-3.43" y1="-6.35" x2="3.43" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="3.43" y1="-6.35" x2="3.43" y2="6.4" width="0.1524" layer="21"/>
<wire x1="3.43" y1="6.4" x2="-3.43" y2="6.4" width="0.1524" layer="21"/>
<text x="-3.048" y="7.112" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-3.4036" y1="6.4262" x2="-3.4036" y2="3.0226" width="0.127" layer="21"/>
<wire x1="-3.429" y1="2.921" x2="3.5814" y2="2.921" width="0.1524" layer="21"/>
<wire x1="3.5814" y1="2.921" x2="3.556" y2="6.35" width="0.1524" layer="21"/>
<wire x1="3.556" y1="6.35" x2="3.81" y2="6.35" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.35" x2="3.81" y2="6.096" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.096" x2="3.81" y2="5.842" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.842" x2="3.81" y2="5.588" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.588" x2="3.81" y2="5.207" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.207" x2="3.81" y2="3.048" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.683" y2="3.048" width="0.1524" layer="21"/>
<wire x1="3.683" y1="3.048" x2="3.683" y2="3.0734" width="0.1524" layer="21"/>
<wire x1="3.683" y1="3.0734" x2="3.683" y2="4.953" width="0.1524" layer="21"/>
<wire x1="3.683" y1="4.953" x2="3.683" y2="6.223" width="0.1524" layer="21"/>
<wire x1="3.683" y1="6.223" x2="4.064" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.064" y1="6.35" x2="3.81" y2="6.096" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.096" x2="4.191" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.191" y1="6.35" x2="3.81" y2="5.842" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.842" x2="4.318" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.318" y1="6.35" x2="3.81" y2="5.588" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.588" x2="4.445" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.445" y1="6.35" x2="3.937" y2="5.588" width="0.1524" layer="21"/>
<wire x1="3.937" y1="5.588" x2="4.572" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.572" y1="6.35" x2="3.937" y2="5.461" width="0.1524" layer="21"/>
<wire x1="3.937" y1="5.461" x2="4.699" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.699" y1="6.35" x2="3.81" y2="5.207" width="0.1524" layer="21"/>
<wire x1="3.81" y1="5.207" x2="4.699" y2="6.096" width="0.1524" layer="21"/>
<wire x1="4.699" y1="6.096" x2="4.953" y2="6.35" width="0.1524" layer="21"/>
<wire x1="4.953" y1="6.35" x2="4.572" y2="6.223" width="0.1524" layer="21"/>
<wire x1="4.572" y1="6.223" x2="4.699" y2="6.096" width="0.1524" layer="21"/>
<wire x1="4.699" y1="6.096" x2="5.334" y2="6.35" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.223" x2="5.969" y2="6.223" width="0.1524" layer="21"/>
<wire x1="4.699" y1="5.969" x2="5.969" y2="6.223" width="0.1524" layer="21"/>
<wire x1="5.969" y1="6.223" x2="4.953" y2="6.096" width="0.1524" layer="21"/>
<wire x1="4.953" y1="6.096" x2="3.937" y2="5.207" width="0.1524" layer="21"/>
<wire x1="3.937" y1="5.207" x2="5.08" y2="5.969" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.842" x2="3.683" y2="4.953" width="0.1524" layer="21"/>
<wire x1="3.683" y1="4.953" x2="5.207" y2="5.842" width="0.1524" layer="21"/>
<wire x1="5.207" y1="5.842" x2="3.937" y2="4.953" width="0.1524" layer="21"/>
<wire x1="3.937" y1="4.953" x2="3.937" y2="4.191" width="0.1524" layer="21"/>
<wire x1="3.937" y1="4.191" x2="3.937" y2="3.302" width="0.1524" layer="21"/>
<wire x1="3.937" y1="3.302" x2="3.937" y2="3.048" width="0.1524" layer="21"/>
<wire x1="3.937" y1="3.048" x2="4.064" y2="4.953" width="0.1524" layer="21"/>
<wire x1="4.064" y1="4.953" x2="5.461" y2="5.842" width="0.1524" layer="21"/>
<wire x1="5.461" y1="5.715" x2="4.064" y2="4.826" width="0.254" layer="21"/>
<wire x1="5.207" y1="5.461" x2="4.191" y2="4.318" width="0.508" layer="21"/>
<wire x1="5.588" y1="4.445" x2="4.953" y2="4.064" width="0.762" layer="21"/>
<wire x1="4.953" y1="4.064" x2="3.937" y2="4.191" width="0.762" layer="21"/>
<wire x1="3.937" y1="4.191" x2="4.191" y2="3.683" width="0.762" layer="21"/>
<wire x1="4.191" y1="3.683" x2="3.937" y2="3.302" width="0.762" layer="21"/>
<wire x1="3.937" y1="3.302" x2="4.699" y2="3.429" width="0.762" layer="21"/>
<wire x1="4.699" y1="3.429" x2="5.334" y2="3.556" width="0.762" layer="21"/>
<wire x1="3.1496" y1="6.4008" x2="5.0038" y2="6.4008" width="0.127" layer="21"/>
<wire x1="5.0038" y1="6.4008" x2="5.0546" y2="6.4008" width="0.127" layer="21"/>
<wire x1="5.0546" y1="6.4008" x2="5.0546" y2="5.9182" width="0.127" layer="21"/>
<wire x1="5.0546" y1="5.9182" x2="4.1656" y2="5.9182" width="0.127" layer="21"/>
<wire x1="4.1656" y1="5.9182" x2="4.1656" y2="4.8006" width="0.127" layer="21"/>
<wire x1="4.1656" y1="4.5974" x2="4.1656" y2="4.8006" width="0.127" layer="21"/>
<wire x1="4.1656" y1="4.8006" x2="5.1816" y2="4.8006" width="0.127" layer="21"/>
<wire x1="5.1816" y1="4.8006" x2="5.1816" y2="5.969" width="0.127" layer="21"/>
<wire x1="5.4356" y1="6.3246" x2="5.4356" y2="6.0452" width="0.127" layer="21"/>
<wire x1="5.9436" y1="6.223" x2="5.9436" y2="6.096" width="0.127" layer="21"/>
<wire x1="5.9436" y1="6.096" x2="5.7658" y2="6.096" width="0.127" layer="21"/>
<wire x1="5.6388" y1="5.7912" x2="5.6388" y2="5.6896" width="0.127" layer="21"/>
<wire x1="5.6388" y1="5.6896" x2="5.5372" y2="5.6896" width="0.127" layer="21"/>
<wire x1="5.5372" y1="5.6896" x2="5.5372" y2="5.8166" width="0.127" layer="21"/>
<wire x1="5.5372" y1="5.6896" x2="5.5372" y2="5.5118" width="0.127" layer="21"/>
<wire x1="5.5372" y1="5.5118" x2="5.461" y2="5.5118" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.6134" x2="5.08" y2="5.0546" width="0.127" layer="21"/>
<wire x1="5.08" y1="4.7752" x2="5.08" y2="5.0546" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.0546" x2="5.588" y2="5.0546" width="0.127" layer="21"/>
<wire x1="5.588" y1="5.0546" x2="5.588" y2="5.1816" width="0.127" layer="21"/>
<wire x1="5.588" y1="5.3594" x2="5.4356" y2="5.3594" width="0.127" layer="21"/>
<wire x1="5.4356" y1="5.3594" x2="5.4356" y2="5.4356" width="0.127" layer="21"/>
<wire x1="5.588" y1="5.3594" x2="5.588" y2="5.1816" width="0.127" layer="21"/>
<wire x1="5.588" y1="5.1816" x2="6.1468" y2="5.1816" width="0.127" layer="21"/>
<wire x1="6.1468" y1="5.1816" x2="6.1468" y2="5.0546" width="0.127" layer="21"/>
<wire x1="6.1468" y1="5.0546" x2="5.6388" y2="5.0546" width="0.127" layer="21"/>
<wire x1="6.1468" y1="5.0546" x2="6.2484" y2="5.0546" width="0.127" layer="21"/>
<wire x1="6.2484" y1="5.0546" x2="6.2484" y2="5.2324" width="0.127" layer="21"/>
<wire x1="6.2484" y1="5.2324" x2="5.334" y2="5.2324" width="0.127" layer="21"/>
<wire x1="5.334" y1="5.2324" x2="5.334" y2="5.1308" width="0.127" layer="21"/>
<wire x1="5.334" y1="5.1308" x2="5.207" y2="5.1308" width="0.127" layer="21"/>
<wire x1="5.334" y1="5.1308" x2="5.5118" y2="5.1308" width="0.127" layer="21"/>
<wire x1="5.5118" y1="5.1308" x2="5.5118" y2="5.1562" width="0.127" layer="21"/>
<wire x1="5.5118" y1="5.1308" x2="5.5118" y2="4.9276" width="0.127" layer="21"/>
<wire x1="5.5118" y1="4.9276" x2="5.2578" y2="4.9276" width="0.127" layer="21"/>
<wire x1="4.445" y1="2.9972" x2="4.445" y2="3.1242" width="0.127" layer="21"/>
<wire x1="5.4356" y1="3.2512" x2="5.4356" y2="3.3528" width="0.127" layer="21"/>
<wire x1="5.0038" y1="6.3754" x2="5.0038" y2="6.4008" width="0.127" layer="21"/>
<wire x1="5.0546" y1="6.4008" x2="6.604" y2="6.4008" width="0.127" layer="21"/>
<wire x1="6.604" y1="6.4008" x2="6.604" y2="2.8956" width="0.127" layer="21"/>
<wire x1="6.604" y1="2.8956" x2="3.683" y2="2.8956" width="0.127" layer="21"/>
<wire x1="3.3782" y1="2.8956" x2="3.683" y2="2.8956" width="0.127" layer="21"/>
<wire x1="3.683" y1="2.8956" x2="3.683" y2="3.0734" width="0.127" layer="21"/>
<wire x1="3.683" y1="3.0734" x2="6.4516" y2="3.0734" width="0.254" layer="21"/>
<wire x1="6.4516" y1="3.0734" x2="6.4516" y2="6.2484" width="0.254" layer="21"/>
<wire x1="6.4516" y1="6.2484" x2="5.5626" y2="6.2484" width="0.254" layer="21"/>
<wire x1="5.5626" y1="6.2484" x2="5.5626" y2="3.0988" width="0.254" layer="21"/>
<wire x1="5.5626" y1="3.0988" x2="5.461" y2="3.0988" width="0.254" layer="21"/>
<wire x1="5.5626" y1="3.0988" x2="6.2484" y2="3.0988" width="0.254" layer="21"/>
<wire x1="6.2484" y1="3.0988" x2="6.2484" y2="6.0452" width="0.254" layer="21"/>
<wire x1="6.2484" y1="6.1468" x2="6.2484" y2="6.0452" width="0.254" layer="21"/>
<wire x1="6.2484" y1="6.0452" x2="5.715" y2="6.0452" width="0.254" layer="21"/>
<wire x1="5.715" y1="6.0452" x2="5.715" y2="3.2766" width="0.254" layer="21"/>
<wire x1="5.715" y1="3.2766" x2="6.0452" y2="3.2766" width="0.254" layer="21"/>
<wire x1="6.0706" y1="3.2766" x2="6.0452" y2="3.2766" width="0.254" layer="21"/>
<wire x1="6.0452" y1="3.2766" x2="6.0452" y2="5.9182" width="0.254" layer="21"/>
<wire x1="6.0452" y1="5.9182" x2="5.8928" y2="5.9182" width="0.254" layer="21"/>
<wire x1="5.8928" y1="5.9182" x2="5.8928" y2="3.3782" width="0.254" layer="21"/>
<wire x1="5.8928" y1="3.3782" x2="5.9182" y2="3.3782" width="0.254" layer="21"/>
<wire x1="5.8928" y1="3.3782" x2="4.8006" y2="3.3782" width="0.254" layer="21"/>
<wire x1="4.8006" y1="3.3782" x2="4.8006" y2="4.826" width="0.254" layer="21"/>
<wire x1="4.8006" y1="4.826" x2="5.0292" y2="4.826" width="0.254" layer="21"/>
<wire x1="5.4102" y1="4.826" x2="5.0292" y2="4.826" width="0.254" layer="21"/>
<wire x1="5.0292" y1="4.826" x2="5.0292" y2="4.699" width="0.254" layer="21"/>
<wire x1="5.0292" y1="4.572" x2="5.0292" y2="4.699" width="0.254" layer="21"/>
<wire x1="5.0292" y1="4.699" x2="5.334" y2="4.699" width="0.254" layer="21"/>
<wire x1="5.334" y1="4.699" x2="5.334" y2="6.0198" width="0.254" layer="21"/>
<wire x1="5.334" y1="6.0198" x2="6.477" y2="6.0198" width="0.254" layer="21"/>
<wire x1="6.477" y1="6.0198" x2="6.477" y2="3.0226" width="0.254" layer="21"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="LED_1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="0" y1="-1.27" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0" x2="-3.048" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="0" x2="-3.048" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0" x2="-3.048" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0" x2="-3.048" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="21"/>
<wire x1="0" y1="-1.27" x2="-0.254" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.254" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-1.778" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.778" y1="1.27" x2="-3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.556" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-0.254" y2="-1.27" width="0.254" layer="21"/>
<wire x1="0" y1="1.27" x2="-0.254" y2="1.27" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CN301_FAB">
<wire x1="-17.78" y1="30.48" x2="17.78" y2="30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="17.78" y1="-35.56" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-17.78" y2="30.48" width="0.254" layer="94"/>
<text x="1.27" y="33.02" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-1.27" y="-38.1" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="DEBUG2" x="-22.86" y="27.94" length="middle" direction="pas"/>
<pin name="DEBUG4" x="-22.86" y="25.4" length="middle" direction="pas"/>
<pin name="DEBUG6" x="-22.86" y="22.86" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO2" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO4" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO6" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="GND_SYS_2" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="5V0_2" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="FAB_OBC_GIO2" x="-22.86" y="7.62" length="middle" direction="pas"/>
<pin name="FAB_OBC_GIO4" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="TEMP_2(-Y)" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="UNREG1_2" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="3V3_2_2" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="TEMP_1(+X)" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="RAW_POWER_2" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="TEMP_5(-X)" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="TEMP_3(-Z)" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="UNREG_2_2" x="-22.86" y="-15.24" length="middle" direction="pas"/>
<pin name="TEMP_4(+Y)" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="DEP_SW1" x="-22.86" y="-20.32" length="middle" direction="pas"/>
<pin name="CPLD1/DEP_SW3" x="-22.86" y="-22.86" length="middle" direction="pas"/>
<pin name="CPLD3" x="-22.86" y="-25.4" length="middle" direction="pas"/>
<pin name="CPLD5" x="-22.86" y="-27.94" length="middle" direction="pas"/>
<pin name="CPLD7" x="-22.86" y="-30.48" length="middle" direction="pas"/>
<pin name="3V3_1_2" x="-22.86" y="-33.02" length="middle" direction="pas"/>
<pin name="DEBUG1" x="22.86" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="DEBUG3" x="22.86" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="DEBUG5" x="22.86" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO1" x="22.86" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO3" x="22.86" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO5" x="22.86" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="GND_SYS_1" x="22.86" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="5V0_1" x="22.86" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_OBC_GIO1" x="22.86" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_OBC_GIO3" x="22.86" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="POWERSC_-Y" x="22.86" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG1_1" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_2_1" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="POWERSC_+X" x="22.86" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="RAW_POWER_1" x="22.86" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="POWERSC_-X" x="22.86" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="POWERSRC_-Z" x="22.86" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG_2_1" x="22.86" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="POWERSC_+Y" x="22.86" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="KILL_SW1" x="22.86" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="DEP_SW2" x="22.86" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD2/DEP_SW4" x="22.86" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD4" x="22.86" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD6" x="22.86" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_1_1" x="22.86" y="-33.02" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="CN305_MSN">
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<text x="1.27" y="35.56" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-1.27" y="-35.56" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="COM_RAB_GIO2" x="-22.86" y="30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO4" x="-22.86" y="27.94" length="middle" direction="pas"/>
<pin name="MTQ_RTN_-X" x="-22.86" y="25.4" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO2" x="-22.86" y="22.86" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO4" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO6" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="GND_SYS_2" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="5V0_2" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="MTQ_RTN_-Z" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="MTQ_RTN_-Y" x="-22.86" y="7.62" length="middle" direction="pas"/>
<pin name="CPLD44" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="UNREG1_2" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="3V3_2_2" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="CPLD46" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="CPLD48" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="CPLD50" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="CPLD52" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="UNREG_2_2" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="CPLD54" x="-22.86" y="-15.24" length="middle" direction="pas"/>
<pin name="CPLD56" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="CPLD58" x="-22.86" y="-20.32" length="middle" direction="pas"/>
<pin name="CPLD60" x="-22.86" y="-22.86" length="middle" direction="pas"/>
<pin name="CPLD62" x="-22.86" y="-25.4" length="middle" direction="pas"/>
<pin name="CPLD64" x="-22.86" y="-27.94" length="middle" direction="pas"/>
<pin name="3V3_1_2" x="-22.86" y="-30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO1" x="22.86" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="COM_RAB_GIO3" x="22.86" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="MTQ_PWR_-X" x="22.86" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO1" x="22.86" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO3" x="22.86" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO5" x="22.86" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND_SYS_1" x="22.86" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="5V0_1" x="22.86" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="MTQ_PWR_-Z" x="22.86" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="MTQ_PWR_-Y" x="22.86" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD43" x="22.86" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG1_1" x="22.86" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_2_1" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD45" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD47" x="22.86" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD49" x="22.86" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD51" x="22.86" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG_2_1" x="22.86" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD53" x="22.86" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD55" x="22.86" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD57" x="22.86" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD59" x="22.86" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD61" x="22.86" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD63" x="22.86" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_1_1" x="22.86" y="-30.48" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="CN302_OBC">
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<text x="1.27" y="35.56" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-1.27" y="-35.56" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="DEBUG2" x="-22.86" y="30.48" length="middle" direction="pas"/>
<pin name="DEBUG4" x="-22.86" y="27.94" length="middle" direction="pas"/>
<pin name="DEBUG6" x="-22.86" y="25.4" length="middle" direction="pas"/>
<pin name="OBC_COM2" x="-22.86" y="22.86" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO4" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO6" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="GND_SYS_2" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="5V0_2" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="FAB_OBC_GIO2" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="FAB_OBC_GIO4" x="-22.86" y="7.62" length="middle" direction="pas"/>
<pin name="CLPD9" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="UNREG1_2" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="3V3_2_2" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="CPLD11" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="RAW_POWER_2" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="CPLD13" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="CPLD15" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="UNREG_2_2" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="CPLD17" x="-22.86" y="-15.24" length="middle" direction="pas"/>
<pin name="DEP_SW1" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="CPLD18" x="-22.86" y="-20.32" length="middle" direction="pas"/>
<pin name="OBC_COM4" x="-22.86" y="-22.86" length="middle" direction="pas"/>
<pin name="OBC_COM6" x="-22.86" y="-25.4" length="middle" direction="pas"/>
<pin name="OBC_COM8" x="-22.86" y="-27.94" length="middle" direction="pas"/>
<pin name="3V3_1_2" x="-22.86" y="-30.48" length="middle" direction="pas"/>
<pin name="DEBUG1" x="22.86" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="DEBUG3" x="22.86" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="DEBUG5" x="22.86" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM1" x="22.86" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO3" x="22.86" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO5" x="22.86" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND_SYS_1" x="22.86" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="5V0_1" x="22.86" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_OBC_GIO1" x="22.86" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_OBC_GIO3" x="22.86" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD8" x="22.86" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG1_1" x="22.86" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_2_1" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD10" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="RAW_POWER_1" x="22.86" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD12" x="22.86" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD14" x="22.86" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG_2_1" x="22.86" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD16" x="22.86" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="KILL_SW1" x="22.86" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="DEP_SW2" x="22.86" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM3" x="22.86" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM5" x="22.86" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM7" x="22.86" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_1_1" x="22.86" y="-30.48" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="CN304_COM">
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<text x="1.27" y="35.56" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-1.27" y="-35.56" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="COM_RAB_GIO2" x="-22.86" y="30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO4" x="-22.86" y="27.94" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO6" x="-22.86" y="25.4" length="middle" direction="pas"/>
<pin name="OBC_COM2" x="-22.86" y="22.86" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO4" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO6" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="GND_SYS_2" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="5V0_2" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="CPLD20" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="CPLD22" x="-22.86" y="7.62" length="middle" direction="pas"/>
<pin name="CPLD24" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="UNREG1_2" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="3V3_2_2" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="CPLD26" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="CPLD28" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="CPLD30" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="CPLD32" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="UNREG_2_2" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="CPLD34" x="-22.86" y="-15.24" length="middle" direction="pas"/>
<pin name="CPLD36" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="CPLD38" x="-22.86" y="-20.32" length="middle" direction="pas"/>
<pin name="OBC_COM4" x="-22.86" y="-22.86" length="middle" direction="pas"/>
<pin name="OBC_COM6" x="-22.86" y="-25.4" length="middle" direction="pas"/>
<pin name="OBC_COM8" x="-22.86" y="-27.94" length="middle" direction="pas"/>
<pin name="3V3_1_2" x="-22.86" y="-30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO1" x="22.86" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="COM_RAB_GIO3" x="22.86" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="COM_RAB_GIO5" x="22.86" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM1" x="22.86" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO3" x="22.86" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO5" x="22.86" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND_SYS_1" x="22.86" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="5V0_1" x="22.86" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD19" x="22.86" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD21" x="22.86" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD23" x="22.86" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG1_1" x="22.86" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_2_1" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD25" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD27" x="22.86" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD29" x="22.86" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD31" x="22.86" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG_2_1" x="22.86" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD33" x="22.86" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD35" x="22.86" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD37" x="22.86" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM3" x="22.86" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM5" x="22.86" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="OBC_COM7" x="22.86" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_1_1" x="22.86" y="-30.48" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="CN306_RAB">
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<text x="1.27" y="35.56" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-1.27" y="-35.56" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="COM_RAB_GIO2" x="-22.86" y="30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO4" x="-22.86" y="27.94" length="middle" direction="pas"/>
<pin name="MTQ_RTN_-X" x="-22.86" y="25.4" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO2" x="-22.86" y="22.86" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO4" x="-22.86" y="20.32" length="middle" direction="pas"/>
<pin name="FAB_RAB_GIO6" x="-22.86" y="17.78" length="middle" direction="pas"/>
<pin name="GND_SYS_2" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="5V0_2" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="CPLD66" x="-22.86" y="10.16" length="middle" direction="pas"/>
<pin name="CPLD68" x="-22.86" y="7.62" length="middle" direction="pas"/>
<pin name="CPLD70" x="-22.86" y="5.08" length="middle" direction="pas"/>
<pin name="UNREG1_2" x="-22.86" y="2.54" length="middle" direction="pas"/>
<pin name="3V3_2_2" x="-22.86" y="0" length="middle" direction="pas"/>
<pin name="CPLD72" x="-22.86" y="-2.54" length="middle" direction="pas"/>
<pin name="CPLD74" x="-22.86" y="-5.08" length="middle" direction="pas"/>
<pin name="CPLD76" x="-22.86" y="-7.62" length="middle" direction="pas"/>
<pin name="CPLD78" x="-22.86" y="-10.16" length="middle" direction="pas"/>
<pin name="UNREG_2_2" x="-22.86" y="-12.7" length="middle" direction="pas"/>
<pin name="CPLD80" x="-22.86" y="-15.24" length="middle" direction="pas"/>
<pin name="CPLD82" x="-22.86" y="-17.78" length="middle" direction="pas"/>
<pin name="CPLD84" x="-22.86" y="-20.32" length="middle" direction="pas"/>
<pin name="CPLD86" x="-22.86" y="-22.86" length="middle" direction="pas"/>
<pin name="CPLD88" x="-22.86" y="-25.4" length="middle" direction="pas"/>
<pin name="CPLD90" x="-22.86" y="-27.94" length="middle" direction="pas"/>
<pin name="3V3_1_2" x="-22.86" y="-30.48" length="middle" direction="pas"/>
<pin name="COM_RAB_GIO1" x="22.86" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="COM_RAB_GIO3" x="22.86" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="MTQ_PWR_-X" x="22.86" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO1" x="22.86" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO3" x="22.86" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FAB_RAB_GIO5" x="22.86" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND_SYS_1" x="22.86" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="5V0_1" x="22.86" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD65" x="22.86" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD67" x="22.86" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD69" x="22.86" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG1_1" x="22.86" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_2_1" x="22.86" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD71" x="22.86" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD73" x="22.86" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD75" x="22.86" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD77" x="22.86" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="UNREG_2_1" x="22.86" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD79" x="22.86" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD81" x="22.86" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD83" x="22.86" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD85" x="22.86" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD87" x="22.86" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="CPLD89" x="22.86" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="3V3_1_1" x="22.86" y="-30.48" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="POWER_PROBE">
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="5.08" x2="1.905" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.905" y1="5.08" x2="1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.905" y1="-5.08" x2="-1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-5.08" x2="-1.905" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="PINH2X2">
<wire x1="-8.89" y1="-5.08" x2="6.35" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="-5.08" width="0.4064" layer="94"/>
<text x="-8.89" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-8.89" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-5.08" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="2.54" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="6" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINH2X6">
<wire x1="-6.35" y1="-10.16" x2="8.89" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X10">
<wire x1="-6.35" y1="-15.24" x2="8.89" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-15.24" x2="8.89" y2="12.7" width="0.4064" layer="94"/>
<wire x1="8.89" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X11">
<wire x1="-6.35" y1="-15.24" x2="8.89" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-15.24" x2="8.89" y2="15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="15.24" x2="-6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="15.24" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="15.875" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X13">
<wire x1="-6.35" y1="-17.78" x2="8.89" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-17.78" x2="8.89" y2="17.78" width="0.4064" layer="94"/>
<wire x1="8.89" y1="17.78" x2="-6.35" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="17.78" x2="-6.35" y2="-17.78" width="0.4064" layer="94"/>
<text x="-6.35" y="18.415" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="HOLE">
<circle x="0" y="0" radius="2.54" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="SPDT">
<wire x1="-3.81" y1="1.905" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="0" width="0.1524" layer="94"/>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="O" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="CN301_FAB">
<gates>
<gate name="G$1" symbol="CN301_FAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SHDR50W60P200_2X25_5000X400X44">
<connects>
<connect gate="G$1" pin="3V3_1_1" pad="49"/>
<connect gate="G$1" pin="3V3_1_2" pad="50"/>
<connect gate="G$1" pin="3V3_2_1" pad="25"/>
<connect gate="G$1" pin="3V3_2_2" pad="26"/>
<connect gate="G$1" pin="5V0_1" pad="15"/>
<connect gate="G$1" pin="5V0_2" pad="16"/>
<connect gate="G$1" pin="CPLD1/DEP_SW3" pad="42"/>
<connect gate="G$1" pin="CPLD2/DEP_SW4" pad="43"/>
<connect gate="G$1" pin="CPLD3" pad="44"/>
<connect gate="G$1" pin="CPLD4" pad="45"/>
<connect gate="G$1" pin="CPLD5" pad="46"/>
<connect gate="G$1" pin="CPLD6" pad="47"/>
<connect gate="G$1" pin="CPLD7" pad="48"/>
<connect gate="G$1" pin="DEBUG1" pad="1"/>
<connect gate="G$1" pin="DEBUG2" pad="2"/>
<connect gate="G$1" pin="DEBUG3" pad="3"/>
<connect gate="G$1" pin="DEBUG4" pad="4"/>
<connect gate="G$1" pin="DEBUG5" pad="5"/>
<connect gate="G$1" pin="DEBUG6" pad="6"/>
<connect gate="G$1" pin="DEP_SW1" pad="40"/>
<connect gate="G$1" pin="DEP_SW2" pad="41"/>
<connect gate="G$1" pin="FAB_OBC_GIO1" pad="17"/>
<connect gate="G$1" pin="FAB_OBC_GIO2" pad="18"/>
<connect gate="G$1" pin="FAB_OBC_GIO3" pad="19"/>
<connect gate="G$1" pin="FAB_OBC_GIO4" pad="20"/>
<connect gate="G$1" pin="FAB_RAB_GIO1" pad="7"/>
<connect gate="G$1" pin="FAB_RAB_GIO2" pad="8"/>
<connect gate="G$1" pin="FAB_RAB_GIO3" pad="9"/>
<connect gate="G$1" pin="FAB_RAB_GIO4" pad="10"/>
<connect gate="G$1" pin="FAB_RAB_GIO5" pad="11"/>
<connect gate="G$1" pin="FAB_RAB_GIO6" pad="12"/>
<connect gate="G$1" pin="GND_SYS_1" pad="13"/>
<connect gate="G$1" pin="GND_SYS_2" pad="14"/>
<connect gate="G$1" pin="KILL_SW1" pad="39"/>
<connect gate="G$1" pin="POWERSC_+X" pad="27"/>
<connect gate="G$1" pin="POWERSC_+Y" pad="37"/>
<connect gate="G$1" pin="POWERSC_-X" pad="31"/>
<connect gate="G$1" pin="POWERSC_-Y" pad="21"/>
<connect gate="G$1" pin="POWERSRC_-Z" pad="33"/>
<connect gate="G$1" pin="RAW_POWER_1" pad="29"/>
<connect gate="G$1" pin="RAW_POWER_2" pad="30"/>
<connect gate="G$1" pin="TEMP_1(+X)" pad="28"/>
<connect gate="G$1" pin="TEMP_2(-Y)" pad="22"/>
<connect gate="G$1" pin="TEMP_3(-Z)" pad="34"/>
<connect gate="G$1" pin="TEMP_4(+Y)" pad="38"/>
<connect gate="G$1" pin="TEMP_5(-X)" pad="32"/>
<connect gate="G$1" pin="UNREG1_1" pad="23"/>
<connect gate="G$1" pin="UNREG1_2" pad="24"/>
<connect gate="G$1" pin="UNREG_2_1" pad="35"/>
<connect gate="G$1" pin="UNREG_2_2" pad="36"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CN305_MSN">
<gates>
<gate name="G$1" symbol="CN305_MSN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SHDR50W60P200_2X25_5000X400X44">
<connects>
<connect gate="G$1" pin="3V3_1_1" pad="49"/>
<connect gate="G$1" pin="3V3_1_2" pad="50"/>
<connect gate="G$1" pin="3V3_2_1" pad="25"/>
<connect gate="G$1" pin="3V3_2_2" pad="26"/>
<connect gate="G$1" pin="5V0_1" pad="15"/>
<connect gate="G$1" pin="5V0_2" pad="16"/>
<connect gate="G$1" pin="COM_RAB_GIO1" pad="1"/>
<connect gate="G$1" pin="COM_RAB_GIO2" pad="2"/>
<connect gate="G$1" pin="COM_RAB_GIO3" pad="3"/>
<connect gate="G$1" pin="COM_RAB_GIO4" pad="4"/>
<connect gate="G$1" pin="CPLD43" pad="21"/>
<connect gate="G$1" pin="CPLD44" pad="22"/>
<connect gate="G$1" pin="CPLD45" pad="27"/>
<connect gate="G$1" pin="CPLD46" pad="28"/>
<connect gate="G$1" pin="CPLD47" pad="29"/>
<connect gate="G$1" pin="CPLD48" pad="30"/>
<connect gate="G$1" pin="CPLD49" pad="31"/>
<connect gate="G$1" pin="CPLD50" pad="32"/>
<connect gate="G$1" pin="CPLD51" pad="33"/>
<connect gate="G$1" pin="CPLD52" pad="34"/>
<connect gate="G$1" pin="CPLD53" pad="37"/>
<connect gate="G$1" pin="CPLD54" pad="38"/>
<connect gate="G$1" pin="CPLD55" pad="39"/>
<connect gate="G$1" pin="CPLD56" pad="40"/>
<connect gate="G$1" pin="CPLD57" pad="41"/>
<connect gate="G$1" pin="CPLD58" pad="42"/>
<connect gate="G$1" pin="CPLD59" pad="43"/>
<connect gate="G$1" pin="CPLD60" pad="44"/>
<connect gate="G$1" pin="CPLD61" pad="45"/>
<connect gate="G$1" pin="CPLD62" pad="46"/>
<connect gate="G$1" pin="CPLD63" pad="47"/>
<connect gate="G$1" pin="CPLD64" pad="48"/>
<connect gate="G$1" pin="FAB_RAB_GIO1" pad="7"/>
<connect gate="G$1" pin="FAB_RAB_GIO2" pad="8"/>
<connect gate="G$1" pin="FAB_RAB_GIO3" pad="9"/>
<connect gate="G$1" pin="FAB_RAB_GIO4" pad="10"/>
<connect gate="G$1" pin="FAB_RAB_GIO5" pad="11"/>
<connect gate="G$1" pin="FAB_RAB_GIO6" pad="12"/>
<connect gate="G$1" pin="GND_SYS_1" pad="13"/>
<connect gate="G$1" pin="GND_SYS_2" pad="14"/>
<connect gate="G$1" pin="MTQ_PWR_-X" pad="5"/>
<connect gate="G$1" pin="MTQ_PWR_-Y" pad="19"/>
<connect gate="G$1" pin="MTQ_PWR_-Z" pad="17"/>
<connect gate="G$1" pin="MTQ_RTN_-X" pad="6"/>
<connect gate="G$1" pin="MTQ_RTN_-Y" pad="20"/>
<connect gate="G$1" pin="MTQ_RTN_-Z" pad="18"/>
<connect gate="G$1" pin="UNREG1_1" pad="23"/>
<connect gate="G$1" pin="UNREG1_2" pad="24"/>
<connect gate="G$1" pin="UNREG_2_1" pad="35"/>
<connect gate="G$1" pin="UNREG_2_2" pad="36"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CN302_OBC">
<gates>
<gate name="G$1" symbol="CN302_OBC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SHDR50W60P200_2X25_5000X400X44">
<connects>
<connect gate="G$1" pin="3V3_1_1" pad="49"/>
<connect gate="G$1" pin="3V3_1_2" pad="50"/>
<connect gate="G$1" pin="3V3_2_1" pad="25"/>
<connect gate="G$1" pin="3V3_2_2" pad="26"/>
<connect gate="G$1" pin="5V0_1" pad="15"/>
<connect gate="G$1" pin="5V0_2" pad="16"/>
<connect gate="G$1" pin="CLPD9" pad="22"/>
<connect gate="G$1" pin="CPLD10" pad="27"/>
<connect gate="G$1" pin="CPLD11" pad="28"/>
<connect gate="G$1" pin="CPLD12" pad="31"/>
<connect gate="G$1" pin="CPLD13" pad="32"/>
<connect gate="G$1" pin="CPLD14" pad="33"/>
<connect gate="G$1" pin="CPLD15" pad="34"/>
<connect gate="G$1" pin="CPLD16" pad="37"/>
<connect gate="G$1" pin="CPLD17" pad="38"/>
<connect gate="G$1" pin="CPLD18" pad="42"/>
<connect gate="G$1" pin="CPLD8" pad="21"/>
<connect gate="G$1" pin="DEBUG1" pad="1"/>
<connect gate="G$1" pin="DEBUG2" pad="2"/>
<connect gate="G$1" pin="DEBUG3" pad="3"/>
<connect gate="G$1" pin="DEBUG4" pad="4"/>
<connect gate="G$1" pin="DEBUG5" pad="5"/>
<connect gate="G$1" pin="DEBUG6" pad="6"/>
<connect gate="G$1" pin="DEP_SW1" pad="40"/>
<connect gate="G$1" pin="DEP_SW2" pad="41"/>
<connect gate="G$1" pin="FAB_OBC_GIO1" pad="17"/>
<connect gate="G$1" pin="FAB_OBC_GIO2" pad="18"/>
<connect gate="G$1" pin="FAB_OBC_GIO3" pad="19"/>
<connect gate="G$1" pin="FAB_OBC_GIO4" pad="20"/>
<connect gate="G$1" pin="FAB_RAB_GIO3" pad="9"/>
<connect gate="G$1" pin="FAB_RAB_GIO4" pad="10"/>
<connect gate="G$1" pin="FAB_RAB_GIO5" pad="11"/>
<connect gate="G$1" pin="FAB_RAB_GIO6" pad="12"/>
<connect gate="G$1" pin="GND_SYS_1" pad="13"/>
<connect gate="G$1" pin="GND_SYS_2" pad="14"/>
<connect gate="G$1" pin="KILL_SW1" pad="39"/>
<connect gate="G$1" pin="OBC_COM1" pad="7"/>
<connect gate="G$1" pin="OBC_COM2" pad="8"/>
<connect gate="G$1" pin="OBC_COM3" pad="43"/>
<connect gate="G$1" pin="OBC_COM4" pad="44"/>
<connect gate="G$1" pin="OBC_COM5" pad="45"/>
<connect gate="G$1" pin="OBC_COM6" pad="46"/>
<connect gate="G$1" pin="OBC_COM7" pad="47"/>
<connect gate="G$1" pin="OBC_COM8" pad="48"/>
<connect gate="G$1" pin="RAW_POWER_1" pad="29"/>
<connect gate="G$1" pin="RAW_POWER_2" pad="30"/>
<connect gate="G$1" pin="UNREG1_1" pad="23"/>
<connect gate="G$1" pin="UNREG1_2" pad="24"/>
<connect gate="G$1" pin="UNREG_2_1" pad="35"/>
<connect gate="G$1" pin="UNREG_2_2" pad="36"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CN304_COM">
<gates>
<gate name="G$1" symbol="CN304_COM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SHDR50W60P200_2X25_5000X400X44">
<connects>
<connect gate="G$1" pin="3V3_1_1" pad="49"/>
<connect gate="G$1" pin="3V3_1_2" pad="50"/>
<connect gate="G$1" pin="3V3_2_1" pad="25"/>
<connect gate="G$1" pin="3V3_2_2" pad="26"/>
<connect gate="G$1" pin="5V0_1" pad="15"/>
<connect gate="G$1" pin="5V0_2" pad="16"/>
<connect gate="G$1" pin="COM_RAB_GIO1" pad="1"/>
<connect gate="G$1" pin="COM_RAB_GIO2" pad="2"/>
<connect gate="G$1" pin="COM_RAB_GIO3" pad="3"/>
<connect gate="G$1" pin="COM_RAB_GIO4" pad="4"/>
<connect gate="G$1" pin="COM_RAB_GIO5" pad="5"/>
<connect gate="G$1" pin="COM_RAB_GIO6" pad="6"/>
<connect gate="G$1" pin="CPLD19" pad="17"/>
<connect gate="G$1" pin="CPLD20" pad="18"/>
<connect gate="G$1" pin="CPLD21" pad="19"/>
<connect gate="G$1" pin="CPLD22" pad="20"/>
<connect gate="G$1" pin="CPLD23" pad="21"/>
<connect gate="G$1" pin="CPLD24" pad="22"/>
<connect gate="G$1" pin="CPLD25" pad="27"/>
<connect gate="G$1" pin="CPLD26" pad="28"/>
<connect gate="G$1" pin="CPLD27" pad="29"/>
<connect gate="G$1" pin="CPLD28" pad="30"/>
<connect gate="G$1" pin="CPLD29" pad="31"/>
<connect gate="G$1" pin="CPLD30" pad="32"/>
<connect gate="G$1" pin="CPLD31" pad="33"/>
<connect gate="G$1" pin="CPLD32" pad="34"/>
<connect gate="G$1" pin="CPLD33" pad="37"/>
<connect gate="G$1" pin="CPLD34" pad="38"/>
<connect gate="G$1" pin="CPLD35" pad="39"/>
<connect gate="G$1" pin="CPLD36" pad="40"/>
<connect gate="G$1" pin="CPLD37" pad="41"/>
<connect gate="G$1" pin="CPLD38" pad="42"/>
<connect gate="G$1" pin="FAB_RAB_GIO3" pad="9"/>
<connect gate="G$1" pin="FAB_RAB_GIO4" pad="10"/>
<connect gate="G$1" pin="FAB_RAB_GIO5" pad="11"/>
<connect gate="G$1" pin="FAB_RAB_GIO6" pad="12"/>
<connect gate="G$1" pin="GND_SYS_1" pad="13"/>
<connect gate="G$1" pin="GND_SYS_2" pad="14"/>
<connect gate="G$1" pin="OBC_COM1" pad="7"/>
<connect gate="G$1" pin="OBC_COM2" pad="8"/>
<connect gate="G$1" pin="OBC_COM3" pad="43"/>
<connect gate="G$1" pin="OBC_COM4" pad="44"/>
<connect gate="G$1" pin="OBC_COM5" pad="45"/>
<connect gate="G$1" pin="OBC_COM6" pad="46"/>
<connect gate="G$1" pin="OBC_COM7" pad="47"/>
<connect gate="G$1" pin="OBC_COM8" pad="48"/>
<connect gate="G$1" pin="UNREG1_1" pad="23"/>
<connect gate="G$1" pin="UNREG1_2" pad="24"/>
<connect gate="G$1" pin="UNREG_2_1" pad="35"/>
<connect gate="G$1" pin="UNREG_2_2" pad="36"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CN306_RAB">
<gates>
<gate name="G$1" symbol="CN306_RAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SHDR50W60P200_2X25_5000X400X44">
<connects>
<connect gate="G$1" pin="3V3_1_1" pad="49"/>
<connect gate="G$1" pin="3V3_1_2" pad="50"/>
<connect gate="G$1" pin="3V3_2_1" pad="25"/>
<connect gate="G$1" pin="3V3_2_2" pad="26"/>
<connect gate="G$1" pin="5V0_1" pad="15"/>
<connect gate="G$1" pin="5V0_2" pad="16"/>
<connect gate="G$1" pin="COM_RAB_GIO1" pad="1"/>
<connect gate="G$1" pin="COM_RAB_GIO2" pad="2"/>
<connect gate="G$1" pin="COM_RAB_GIO3" pad="3"/>
<connect gate="G$1" pin="COM_RAB_GIO4" pad="4"/>
<connect gate="G$1" pin="CPLD65" pad="17"/>
<connect gate="G$1" pin="CPLD66" pad="18"/>
<connect gate="G$1" pin="CPLD67" pad="19"/>
<connect gate="G$1" pin="CPLD68" pad="20"/>
<connect gate="G$1" pin="CPLD69" pad="21"/>
<connect gate="G$1" pin="CPLD70" pad="22"/>
<connect gate="G$1" pin="CPLD71" pad="27"/>
<connect gate="G$1" pin="CPLD72" pad="28"/>
<connect gate="G$1" pin="CPLD73" pad="29"/>
<connect gate="G$1" pin="CPLD74" pad="30"/>
<connect gate="G$1" pin="CPLD75" pad="31"/>
<connect gate="G$1" pin="CPLD76" pad="32"/>
<connect gate="G$1" pin="CPLD77" pad="33"/>
<connect gate="G$1" pin="CPLD78" pad="34"/>
<connect gate="G$1" pin="CPLD79" pad="37"/>
<connect gate="G$1" pin="CPLD80" pad="38"/>
<connect gate="G$1" pin="CPLD81" pad="39"/>
<connect gate="G$1" pin="CPLD82" pad="40"/>
<connect gate="G$1" pin="CPLD83" pad="41"/>
<connect gate="G$1" pin="CPLD84" pad="42"/>
<connect gate="G$1" pin="CPLD85" pad="43"/>
<connect gate="G$1" pin="CPLD86" pad="44"/>
<connect gate="G$1" pin="CPLD87" pad="45"/>
<connect gate="G$1" pin="CPLD88" pad="46"/>
<connect gate="G$1" pin="CPLD89" pad="47"/>
<connect gate="G$1" pin="CPLD90" pad="48"/>
<connect gate="G$1" pin="FAB_RAB_GIO1" pad="7"/>
<connect gate="G$1" pin="FAB_RAB_GIO2" pad="8"/>
<connect gate="G$1" pin="FAB_RAB_GIO3" pad="9"/>
<connect gate="G$1" pin="FAB_RAB_GIO4" pad="10"/>
<connect gate="G$1" pin="FAB_RAB_GIO5" pad="11"/>
<connect gate="G$1" pin="FAB_RAB_GIO6" pad="12"/>
<connect gate="G$1" pin="GND_SYS_1" pad="13"/>
<connect gate="G$1" pin="GND_SYS_2" pad="14"/>
<connect gate="G$1" pin="MTQ_PWR_-X" pad="5"/>
<connect gate="G$1" pin="MTQ_RTN_-X" pad="6"/>
<connect gate="G$1" pin="UNREG1_1" pad="23"/>
<connect gate="G$1" pin="UNREG1_2" pad="24"/>
<connect gate="G$1" pin="UNREG_2_1" pad="35"/>
<connect gate="G$1" pin="UNREG_2_2" pad="36"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER_PROBE">
<gates>
<gate name="G$1" symbol="POWER_PROBE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWER_PROBE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X2_REV" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X02">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X6_REV">
<gates>
<gate name="G$1" symbol="PINH2X6" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="2X06_REV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X10_REV">
<gates>
<gate name="G$1" symbol="PINH2X10" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="2X10_REV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X11_REV">
<gates>
<gate name="G$1" symbol="PINH2X11" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="2X11_REV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X13_REV">
<gates>
<gate name="G$1" symbol="PINH2X13" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="2X13_REV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.2_HOLES">
<gates>
<gate name="G$1" symbol="HOLE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="P$1" pad="B3,2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPDT-7347066">
<gates>
<gate name="G$1" symbol="SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7347066">
<connects>
<connect gate="G$1" pin="O" pad="1"/>
<connect gate="G$1" pin="P" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_0805(1206)">
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_0805(1206)">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Interfaces">
<packages>
<package name="STB2">
<description>&lt;b&gt;MKDS 1/ 2-3,5&lt;/b&gt; Printklemme&lt;p&gt;
Nennstrom: 10 A&lt;br&gt;
Bemessungsspannung: 160 V&lt;br&gt;
Raster: 3,5 mm&lt;br&gt;
Polzahl: 2&lt;br&gt;
Montageart: Löten&lt;br&gt;
Anschlussart: Schraubanschluss&lt;br&gt;
Anschlussrichtung vom Leiter zur Platine: 0°&lt;br&gt;
Source: http://eshop.phoenixcontact.com .. 1751248.pdf</description>
<wire x1="-3.75" y1="3.65" x2="3.75" y2="3.65" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="3.65" x2="-3.75" y2="1.9123" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-2.0993" x2="-3.75" y2="-2.0993" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-1.6163" x2="3.75" y2="1.4367" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-2.0993" x2="3.75" y2="-2.911" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-1.6163" x2="3.75" y2="-2.0993" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="1.9123" x2="-3.75" y2="1.4367" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="1.9123" x2="3.75" y2="1.9123" width="0.1016" layer="21"/>
<wire x1="3.75" y1="1.4367" x2="3.75" y2="1.9123" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-1.6163" x2="-3.75" y2="1.4367" width="0.1016" layer="21"/>
<wire x1="3.75" y1="1.4367" x2="-3.75" y2="1.4367" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-1.6163" x2="3.75" y2="-1.6163" width="0.1016" layer="21"/>
<wire x1="3.75" y1="3.65" x2="3.75" y2="1.9123" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-1.6163" x2="-3.75" y2="-2.0993" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-2.0993" x2="-3.75" y2="-2.911" width="0.1016" layer="21"/>
<wire x1="-2.956" y1="-0.811" x2="-1.039" y2="1.106" width="0.1016" layer="51"/>
<wire x1="3.75" y1="-2.911" x2="-3.75" y2="-2.911" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-3.65" x2="3.75" y2="-3.65" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-3.35" x2="3.75" y2="-3.65" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-3.35" x2="3.75" y2="-3.35" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-3.35" x2="-3.75" y2="-3.65" width="0.1016" layer="21"/>
<wire x1="-3.75" y1="-2.911" x2="-3.75" y2="-3.35" width="0.1016" layer="21"/>
<wire x1="3.75" y1="-2.911" x2="3.75" y2="-3.35" width="0.1016" layer="21"/>
<wire x1="-0.544" y1="0.611" x2="-0.55" y2="0.605" width="0.1016" layer="21"/>
<wire x1="-0.55" y1="0.605" x2="-2.461" y2="-1.306" width="0.1016" layer="51"/>
<wire x1="0.544" y1="-0.811" x2="2.461" y2="1.106" width="0.1016" layer="51"/>
<wire x1="2.956" y1="0.611" x2="2.95" y2="0.605" width="0.1016" layer="21"/>
<wire x1="2.95" y1="0.605" x2="1.039" y2="-1.306" width="0.1016" layer="51"/>
<circle x="-1.75" y="-0.1" radius="1.45" width="0.1016" layer="21"/>
<circle x="1.75" y="-0.1" radius="1.45" width="0.1016" layer="21"/>
<pad name="1" x="-1.75" y="-0.1" drill="1.1" diameter="1.7" rot="R90"/>
<pad name="2" x="1.75" y="-0.1" drill="1.1" diameter="1.7" rot="R90"/>
<text x="-2.2" y="2.2225" size="1.27" layer="21" font="vector">1</text>
<text x="1.2925" y="2.2225" size="1.27" layer="21" font="vector">2</text>
<text x="-4.105" y="-2.8575" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<polygon width="0.1016" layer="21">
<vertex x="-3.1525" y="-2.8575"/>
<vertex x="-3.47" y="-2.2225"/>
<vertex x="-2.835" y="-2.2225"/>
</polygon>
</package>
<package name="S2B-EH">
<pad name="1" x="-1.25" y="0" drill="1"/>
<pad name="2" x="1.25" y="0" drill="1"/>
<wire x1="-3.75" y1="-1.5" x2="-3.75" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-3.75" y1="0.75" x2="-3.75" y2="6.7" width="0.1524" layer="21"/>
<wire x1="3.75" y1="-1.5" x2="3.75" y2="6.7" width="0.1524" layer="21"/>
<wire x1="-3.75" y1="6.7" x2="-3" y2="6.7" width="0.1524" layer="21"/>
<wire x1="-3" y1="6.7" x2="3" y2="6.7" width="0.1524" layer="21"/>
<wire x1="3" y1="6.7" x2="3.75" y2="6.7" width="0.1524" layer="21"/>
<wire x1="-3.75" y1="-1.5" x2="-3" y2="-1.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="0.7" width="0.1524" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="0.7" width="0.1524" layer="21"/>
<wire x1="3.75" y1="-1.5" x2="3" y2="-1.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.5" x2="3" y2="1.5" width="0.1524" layer="21"/>
<wire x1="-3" y1="1.5" x2="-3" y2="6.7" width="0.1524" layer="21"/>
<wire x1="3" y1="1.5" x2="3" y2="6.7" width="0.1524" layer="21"/>
<polygon width="0.1" layer="21">
<vertex x="-1.5" y="1.5"/>
<vertex x="-1.5" y="5.5"/>
<vertex x="-1" y="5.5"/>
<vertex x="-1" y="1.5"/>
</polygon>
<polygon width="0.1" layer="21">
<vertex x="1" y="1.5"/>
<vertex x="1" y="5.5"/>
<vertex x="1.5" y="5.5"/>
<vertex x="1.5" y="1.5"/>
</polygon>
<text x="0" y="-2" size="1.5" layer="25" font="vector" align="center">&gt;NAME</text>
<circle x="-1.25" y="0" radius="1" width="0.1524" layer="21"/>
<circle x="1.25" y="0" radius="1" width="0.1524" layer="21"/>
<wire x1="-3.75" y1="0.75" x2="-2" y2="0.75" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0.75" x2="0.5" y2="0.75" width="0.1524" layer="21"/>
<wire x1="2" y1="0.75" x2="3" y2="0.75" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PIN">
<pin name="PIN" x="0" y="0" visible="pad" length="middle"/>
<wire x1="0" y1="1.27" x2="15.24" y2="1.27" width="0.1" layer="94"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="-1.27" width="0.1" layer="94"/>
<wire x1="15.24" y1="-1.27" x2="0" y2="-1.27" width="0.1" layer="94"/>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="10.16" y="0" size="1.27" layer="95" align="center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PT1.52-3.5-H" prefix="STB">
<description>COMBICON compact
Screw Compact Terminal Blocks
PT(A) 1,5/… with a 3.5 mm Pitch</description>
<gates>
<gate name="P1" symbol="PIN" x="0" y="0"/>
<gate name="P2" symbol="PIN" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="STB2">
<connects>
<connect gate="P1" pin="PIN" pad="1"/>
<connect gate="P2" pin="PIN" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="S2B-EH" prefix="CON">
<gates>
<gate name="P1" symbol="PIN" x="-5.08" y="0"/>
<gate name="P2" symbol="PIN" x="-5.08" y="-5.08"/>
</gates>
<devices>
<device name="" package="S2B-EH">
<connects>
<connect gate="P1" pin="PIN" pad="1"/>
<connect gate="P2" pin="PIN" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="FAB" library="CN30x _EGSE" deviceset="CN301_FAB" device=""/>
<part name="OBC" library="CN30x _EGSE" deviceset="CN302_OBC" device=""/>
<part name="COM" library="CN30x _EGSE" deviceset="CN304_COM" device=""/>
<part name="MSN_I" library="CN30x _EGSE" deviceset="CN305_MSN" device=""/>
<part name="MSN_II" library="CN30x _EGSE" deviceset="CN305_MSN" device=""/>
<part name="RAB" library="CN30x _EGSE" deviceset="CN306_RAB" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="STB1" library="Interfaces" deviceset="PT1.52-3.5-H" device=""/>
<part name="CON1" library="Interfaces" deviceset="S2B-EH" device=""/>
<part name="CON2" library="Interfaces" deviceset="S2B-EH" device=""/>
<part name="CON3" library="Interfaces" deviceset="S2B-EH" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="3V3_1" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="3V3_2" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="UNREG1" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="UNREG2" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="5V" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="RAW_PWR" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="LED1" library="CN30x _EGSE" deviceset="LED_0805(1206)" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="CN30x _EGSE" deviceset="RES_0805(1206)" device="" value="4.7k"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="+X" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="-X" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="+Y" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="-Y" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="-Z" library="CN30x _EGSE" deviceset="POWER_PROBE" device=""/>
<part name="JP1" library="CN30x _EGSE" deviceset="PINHD-2X2_REV" device="" value="FAB"/>
<part name="JP2" library="CN30x _EGSE" deviceset="PINHD-2X6_REV" device="" value="OBC"/>
<part name="JP3" library="CN30x _EGSE" deviceset="PINHD-2X10_REV" device="" value="COM"/>
<part name="JP4" library="CN30x _EGSE" deviceset="PINHD-2X11_REV" device="" value="MSN"/>
<part name="JP5" library="CN30x _EGSE" deviceset="PINHD-2X13_REV" device="" value="RAB"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="HOLE1" library="CN30x _EGSE" deviceset="3.2_HOLES" device=""/>
<part name="HOLE2" library="CN30x _EGSE" deviceset="3.2_HOLES" device=""/>
<part name="HOLE3" library="CN30x _EGSE" deviceset="3.2_HOLES" device=""/>
<part name="HOLE4" library="CN30x _EGSE" deviceset="3.2_HOLES" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SW1" library="CN30x _EGSE" deviceset="SPDT-7347066" device=""/>
<part name="SW2" library="CN30x _EGSE" deviceset="SPDT-7347066" device=""/>
<part name="SW3" library="CN30x _EGSE" deviceset="SPDT-7347066" device=""/>
<part name="SW4" library="CN30x _EGSE" deviceset="SPDT-7347066" device=""/>
<part name="SW5" library="CN30x _EGSE" deviceset="SPDT-7347066" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="299.72" y="11.43" size="2.54" layer="94">BIRDS-3 TESTBED</text>
<text x="346.71" y="19.05" size="2.54" layer="94">Abhas</text>
<text x="312.42" y="228.6" size="2.54" layer="94">S&amp;F MISSION BOARD</text>
<wire x1="228.6" y1="127" x2="228.6" y2="33.02" width="0.1524" layer="94"/>
<wire x1="228.6" y1="33.02" x2="355.6" y2="33.02" width="0.1524" layer="94"/>
<wire x1="355.6" y1="33.02" x2="355.6" y2="127" width="0.1524" layer="94"/>
<wire x1="355.6" y1="127" x2="228.6" y2="127" width="0.1524" layer="94"/>
<text x="50.8" y="121.92" size="2.54" layer="94">ADCS &amp; CAM MISSION BOARD</text>
<text x="269.24" y="35.56" size="5.08" layer="94">CPLD JUMPERS</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="-1.27" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="13.97" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="8.89" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="3.81" size="2.54" layer="94"/>
</instance>
<instance part="FAB" gate="G$1" x="71.12" y="190.5" smashed="yes">
<attribute name="NAME" x="72.39" y="223.52" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="69.85" y="152.4" size="1.778" layer="96" align="center"/>
</instance>
<instance part="OBC" gate="G$1" x="154.94" y="187.96" smashed="yes">
<attribute name="NAME" x="156.21" y="223.52" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="153.67" y="152.4" size="1.778" layer="96" align="center"/>
</instance>
<instance part="COM" gate="G$1" x="241.3" y="187.96" smashed="yes">
<attribute name="NAME" x="242.57" y="223.52" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="240.03" y="152.4" size="1.778" layer="96" align="center"/>
</instance>
<instance part="MSN_I" gate="G$1" x="327.66" y="187.96" smashed="yes">
<attribute name="NAME" x="328.93" y="223.52" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="326.39" y="152.4" size="1.778" layer="96" align="center"/>
</instance>
<instance part="MSN_II" gate="G$1" x="71.12" y="81.28" smashed="yes">
<attribute name="NAME" x="72.39" y="116.84" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="69.85" y="45.72" size="1.778" layer="96" align="center"/>
</instance>
<instance part="RAB" gate="G$1" x="157.48" y="81.28" smashed="yes">
<attribute name="NAME" x="158.75" y="116.84" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="156.21" y="45.72" size="1.778" layer="96" align="center"/>
</instance>
<instance part="JP1" gate="A" x="264.16" y="114.3" smashed="yes">
<attribute name="NAME" x="255.27" y="120.015" size="1.778" layer="95"/>
<attribute name="VALUE" x="255.27" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="261.62" y="93.98" smashed="yes">
<attribute name="NAME" x="255.27" y="102.235" size="1.778" layer="95"/>
<attribute name="VALUE" x="255.27" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="JP3" gate="G$1" x="261.62" y="63.5" smashed="yes">
<attribute name="NAME" x="255.27" y="76.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="255.27" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="JP4" gate="G$1" x="320.04" y="106.68" smashed="yes">
<attribute name="NAME" x="313.69" y="122.555" size="1.778" layer="95"/>
<attribute name="VALUE" x="313.69" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="JP5" gate="G$1" x="320.04" y="63.5" smashed="yes">
<attribute name="NAME" x="313.69" y="81.915" size="1.778" layer="95"/>
<attribute name="VALUE" x="313.69" y="43.18" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
<bus name="DEBUG[1..6],FAB_RAB_GIO[1..6],GND,5V,OBC_COM[1..2],COM_RAB_GIO[1..6],FAB_OBC_GIO[1..4],PWRSC_+X,TEMP_2(+X),UNREG1,UNREG2,3V3_1,3V3_2,PWRSC_+Y,PWRSC_-Y,PWRSC_-Z,PWRSC_-X,TEMP_1(+Y),TEMP_3(-Z),TEMP_4(-X),TEMP_5(-Y),RAW_PWR,DEP_SW[1..4],CPLD[1..90],KILL_SW,OBC_COM[1..8],MTQ_PWR_+X,MTQ_RTN_+X,MTQ_PWR_+Y,MTQ_RTN_+Y,MTQ_PWR_-Z,MTQ_RTN_-Z">
<segment>
<wire x1="30.48" y1="229.87" x2="114.3" y2="229.87" width="0.762" layer="92"/>
<wire x1="114.3" y1="229.87" x2="114.3" y2="147.32" width="0.762" layer="92"/>
<wire x1="114.3" y1="147.32" x2="198.12" y2="147.32" width="0.762" layer="92"/>
<wire x1="198.12" y1="147.32" x2="198.12" y2="231.14" width="0.762" layer="92"/>
<wire x1="198.12" y1="231.14" x2="284.48" y2="231.14" width="0.762" layer="92"/>
<wire x1="284.48" y1="231.14" x2="284.48" y2="147.32" width="0.762" layer="92"/>
<wire x1="284.48" y1="147.32" x2="370.84" y2="147.32" width="0.762" layer="92"/>
<wire x1="370.84" y1="147.32" x2="370.84" y2="228.6" width="0.762" layer="92"/>
<wire x1="30.48" y1="229.87" x2="30.48" y2="30.48" width="0.762" layer="92"/>
<wire x1="30.48" y1="30.48" x2="114.3" y2="30.48" width="0.762" layer="92"/>
<wire x1="114.3" y1="30.48" x2="114.3" y2="127" width="0.762" layer="92"/>
<wire x1="114.3" y1="127" x2="200.66" y2="127" width="0.762" layer="92"/>
<wire x1="200.66" y1="127" x2="200.66" y2="27.94" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="FAB_RAB_GIO2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO2"/>
<wire x1="48.26" y1="210.82" x2="30.48" y2="210.82" width="0.1524" layer="91"/>
<label x="30.48" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO2"/>
<wire x1="304.8" y1="210.82" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<label x="284.48" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO2"/>
<wire x1="48.26" y1="104.14" x2="30.48" y2="104.14" width="0.1524" layer="91"/>
<label x="30.48" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO2"/>
<wire x1="134.62" y1="104.14" x2="114.3" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_RAB_GIO1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO1"/>
<wire x1="93.98" y1="210.82" x2="114.3" y2="210.82" width="0.1524" layer="91"/>
<label x="93.98" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO1"/>
<wire x1="93.98" y1="104.14" x2="114.3" y2="104.14" width="0.1524" layer="91"/>
<label x="93.98" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO1"/>
<wire x1="180.34" y1="104.14" x2="200.66" y2="104.14" width="0.1524" layer="91"/>
<label x="180.34" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO1"/>
<wire x1="350.52" y1="210.82" x2="370.84" y2="210.82" width="0.1524" layer="91"/>
<label x="350.52" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_RAB_GIO3" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="93.98" y1="208.28" x2="114.3" y2="208.28" width="0.1524" layer="91"/>
<label x="93.98" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="177.8" y1="208.28" x2="198.12" y2="208.28" width="0.1524" layer="91"/>
<label x="180.34" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="264.16" y1="208.28" x2="284.48" y2="208.28" width="0.1524" layer="91"/>
<label x="264.16" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="350.52" y1="208.28" x2="370.84" y2="208.28" width="0.1524" layer="91"/>
<label x="350.52" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="93.98" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<label x="93.98" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO3"/>
<wire x1="180.34" y1="101.6" x2="200.66" y2="101.6" width="0.1524" layer="91"/>
<label x="180.34" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_RAB_GIO5" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="93.98" y1="205.74" x2="114.3" y2="205.74" width="0.1524" layer="91"/>
<label x="93.98" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="177.8" y1="205.74" x2="198.12" y2="205.74" width="0.1524" layer="91"/>
<label x="180.34" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="264.16" y1="205.74" x2="284.48" y2="205.74" width="0.1524" layer="91"/>
<label x="264.16" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="350.52" y1="205.74" x2="370.84" y2="205.74" width="0.1524" layer="91"/>
<label x="350.52" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="93.98" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<label x="93.98" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO5"/>
<wire x1="180.34" y1="99.06" x2="200.66" y2="99.06" width="0.1524" layer="91"/>
<label x="180.34" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_RAB_GIO4" class="0">
<segment>
<wire x1="30.48" y1="207.01" x2="30.48" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="48.26" y1="208.28" x2="30.48" y2="208.28" width="0.1524" layer="91"/>
<label x="30.48" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="132.08" y1="208.28" x2="114.3" y2="208.28" width="0.1524" layer="91"/>
<label x="114.3" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="218.44" y1="208.28" x2="198.12" y2="208.28" width="0.1524" layer="91"/>
<label x="198.12" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="304.8" y1="208.28" x2="284.48" y2="208.28" width="0.1524" layer="91"/>
<label x="284.48" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="48.26" y1="101.6" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<label x="30.48" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO4"/>
<wire x1="134.62" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_RAB_GIO6" class="0">
<segment>
<wire x1="30.48" y1="204.47" x2="30.48" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="48.26" y1="205.74" x2="30.48" y2="205.74" width="0.1524" layer="91"/>
<label x="30.48" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="132.08" y1="205.74" x2="114.3" y2="205.74" width="0.1524" layer="91"/>
<label x="114.3" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="218.44" y1="205.74" x2="198.12" y2="205.74" width="0.1524" layer="91"/>
<label x="198.12" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="304.8" y1="205.74" x2="284.48" y2="205.74" width="0.1524" layer="91"/>
<label x="284.48" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="48.26" y1="99.06" x2="30.48" y2="99.06" width="0.1524" layer="91"/>
<label x="30.48" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="FAB_RAB_GIO6"/>
<wire x1="134.62" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<label x="114.3" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM1" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM1"/>
<wire x1="177.8" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<label x="180.34" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM1"/>
<wire x1="264.16" y1="210.82" x2="284.48" y2="210.82" width="0.1524" layer="91"/>
<label x="264.16" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM_RAB_GIO2" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO2"/>
<wire x1="218.44" y1="218.44" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<label x="198.12" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="COM_RAB_GIO2"/>
<wire x1="304.8" y1="218.44" x2="284.48" y2="218.44" width="0.1524" layer="91"/>
<label x="284.48" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="COM_RAB_GIO2"/>
<wire x1="48.26" y1="111.76" x2="30.48" y2="111.76" width="0.1524" layer="91"/>
<label x="30.48" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="COM_RAB_GIO2"/>
<wire x1="134.62" y1="111.76" x2="114.3" y2="111.76" width="0.1524" layer="91"/>
<label x="114.3" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM_RAB_GIO4" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO4"/>
<wire x1="218.44" y1="215.9" x2="198.12" y2="215.9" width="0.1524" layer="91"/>
<label x="198.12" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="COM_RAB_GIO4"/>
<wire x1="304.8" y1="215.9" x2="284.48" y2="215.9" width="0.1524" layer="91"/>
<label x="284.48" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="COM_RAB_GIO4"/>
<wire x1="134.62" y1="109.22" x2="114.3" y2="109.22" width="0.1524" layer="91"/>
<label x="114.3" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="COM_RAB_GIO4"/>
<wire x1="48.26" y1="109.22" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<label x="30.48" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM_RAB_GIO6" class="0">
<segment>
<wire x1="30.48" y1="201.93" x2="30.48" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO6"/>
<wire x1="218.44" y1="213.36" x2="198.12" y2="213.36" width="0.1524" layer="91"/>
<label x="198.12" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM_RAB_GIO1" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO1"/>
<wire x1="264.16" y1="218.44" x2="284.48" y2="218.44" width="0.1524" layer="91"/>
<label x="264.16" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="COM_RAB_GIO1"/>
<wire x1="350.52" y1="218.44" x2="370.84" y2="218.44" width="0.1524" layer="91"/>
<label x="350.52" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="COM_RAB_GIO1"/>
<wire x1="93.98" y1="111.76" x2="114.3" y2="111.76" width="0.1524" layer="91"/>
<wire x1="114.3" y1="111.76" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<label x="93.98" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="COM_RAB_GIO1"/>
<wire x1="180.34" y1="111.76" x2="200.66" y2="111.76" width="0.1524" layer="91"/>
<label x="180.34" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="COM_RAB_GIO3" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO3"/>
<wire x1="264.16" y1="215.9" x2="284.48" y2="215.9" width="0.1524" layer="91"/>
<label x="264.16" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="COM_RAB_GIO3"/>
<wire x1="350.52" y1="215.9" x2="370.84" y2="215.9" width="0.1524" layer="91"/>
<label x="350.52" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="COM_RAB_GIO3"/>
<wire x1="93.98" y1="109.22" x2="114.3" y2="109.22" width="0.1524" layer="91"/>
<label x="93.98" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="COM_RAB_GIO3"/>
<wire x1="180.34" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<label x="180.34" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_OBC_GIO2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_OBC_GIO2"/>
<wire x1="48.26" y1="198.12" x2="30.48" y2="198.12" width="0.1524" layer="91"/>
<label x="30.48" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_OBC_GIO2"/>
<wire x1="132.08" y1="198.12" x2="114.3" y2="198.12" width="0.1524" layer="91"/>
<label x="114.3" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_OBC_GIO4" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_OBC_GIO4"/>
<wire x1="48.26" y1="195.58" x2="30.48" y2="195.58" width="0.1524" layer="91"/>
<label x="30.48" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_OBC_GIO4"/>
<wire x1="132.08" y1="195.58" x2="114.3" y2="195.58" width="0.1524" layer="91"/>
<label x="114.3" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_OBC_GIO1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_OBC_GIO1"/>
<wire x1="93.98" y1="198.12" x2="114.3" y2="198.12" width="0.1524" layer="91"/>
<label x="93.98" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_OBC_GIO1"/>
<wire x1="177.8" y1="198.12" x2="198.12" y2="198.12" width="0.1524" layer="91"/>
<label x="180.34" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAB_OBC_GIO3" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="FAB_OBC_GIO3"/>
<wire x1="93.98" y1="195.58" x2="114.3" y2="195.58" width="0.1524" layer="91"/>
<label x="93.98" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="FAB_OBC_GIO3"/>
<wire x1="177.8" y1="195.58" x2="198.12" y2="195.58" width="0.1524" layer="91"/>
<label x="180.34" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEP_SW1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEP_SW1"/>
<wire x1="48.26" y1="170.18" x2="30.48" y2="170.18" width="0.1524" layer="91"/>
<label x="30.48" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEP_SW1"/>
<wire x1="132.08" y1="170.18" x2="114.3" y2="170.18" width="0.1524" layer="91"/>
<label x="114.3" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEP_SW2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEP_SW2"/>
<wire x1="93.98" y1="167.64" x2="114.3" y2="167.64" width="0.1524" layer="91"/>
<label x="93.98" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEP_SW2"/>
<wire x1="177.8" y1="167.64" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<label x="180.34" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEP_SW3" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD1/DEP_SW3"/>
<wire x1="48.26" y1="167.64" x2="30.48" y2="167.64" width="0.1524" layer="91"/>
<label x="30.48" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEP_SW4" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD2/DEP_SW4"/>
<wire x1="93.98" y1="165.1" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<label x="93.98" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="GND_SYS_2"/>
<wire x1="48.26" y1="203.2" x2="30.48" y2="203.2" width="0.1524" layer="91"/>
<label x="33.02" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="GND_SYS_1"/>
<wire x1="93.98" y1="203.2" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<label x="93.98" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="GND_SYS_1"/>
<wire x1="177.8" y1="203.2" x2="198.12" y2="203.2" width="0.1524" layer="91"/>
<label x="180.34" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="GND_SYS_2"/>
<wire x1="132.08" y1="203.2" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
<label x="116.84" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="GND_SYS_2"/>
<wire x1="218.44" y1="203.2" x2="198.12" y2="203.2" width="0.1524" layer="91"/>
<label x="200.66" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="GND_SYS_1"/>
<wire x1="264.16" y1="203.2" x2="284.48" y2="203.2" width="0.1524" layer="91"/>
<label x="264.16" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="GND_SYS_2"/>
<wire x1="304.8" y1="203.2" x2="284.48" y2="203.2" width="0.1524" layer="91"/>
<label x="287.02" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="GND_SYS_1"/>
<wire x1="350.52" y1="203.2" x2="370.84" y2="203.2" width="0.1524" layer="91"/>
<label x="350.52" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="GND_SYS_1"/>
<wire x1="93.98" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<label x="93.98" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="GND_SYS_2"/>
<wire x1="48.26" y1="96.52" x2="30.48" y2="96.52" width="0.1524" layer="91"/>
<label x="33.02" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="GND_SYS_2"/>
<wire x1="134.62" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<label x="116.84" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="GND_SYS_1"/>
<wire x1="180.34" y1="96.52" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<label x="182.88" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="5V0_2"/>
<wire x1="48.26" y1="200.66" x2="30.48" y2="200.66" width="0.1524" layer="91"/>
<label x="33.02" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="5V0_1"/>
<wire x1="93.98" y1="200.66" x2="114.3" y2="200.66" width="0.1524" layer="91"/>
<label x="93.98" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="5V0_1"/>
<wire x1="177.8" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<label x="180.34" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="5V0_2"/>
<wire x1="132.08" y1="200.66" x2="114.3" y2="200.66" width="0.1524" layer="91"/>
<label x="116.84" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="5V0_2"/>
<wire x1="218.44" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<label x="200.66" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="5V0_1"/>
<wire x1="264.16" y1="200.66" x2="284.48" y2="200.66" width="0.1524" layer="91"/>
<label x="264.16" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="5V0_2"/>
<wire x1="304.8" y1="200.66" x2="284.48" y2="200.66" width="0.1524" layer="91"/>
<label x="287.02" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="5V0_1"/>
<wire x1="350.52" y1="200.66" x2="370.84" y2="200.66" width="0.1524" layer="91"/>
<label x="350.52" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="5V0_1"/>
<wire x1="93.98" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<label x="93.98" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="5V0_2"/>
<wire x1="48.26" y1="93.98" x2="30.48" y2="93.98" width="0.1524" layer="91"/>
<label x="33.02" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="5V0_2"/>
<wire x1="134.62" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<label x="116.84" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="5V0_1"/>
<wire x1="180.34" y1="93.98" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<label x="182.88" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="TEMP_2(+X)" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="TEMP_2(-Y)"/>
<wire x1="48.26" y1="193.04" x2="30.48" y2="193.04" width="0.1524" layer="91"/>
<label x="30.48" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="UNREG1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="UNREG1_2"/>
<wire x1="48.26" y1="190.5" x2="30.48" y2="190.5" width="0.1524" layer="91"/>
<label x="33.02" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="UNREG1_1"/>
<wire x1="93.98" y1="190.5" x2="114.3" y2="190.5" width="0.1524" layer="91"/>
<label x="93.98" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="UNREG1_1"/>
<wire x1="177.8" y1="190.5" x2="198.12" y2="190.5" width="0.1524" layer="91"/>
<label x="180.34" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="UNREG1_2"/>
<wire x1="132.08" y1="190.5" x2="114.3" y2="190.5" width="0.1524" layer="91"/>
<label x="116.84" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="UNREG1_2"/>
<wire x1="218.44" y1="190.5" x2="198.12" y2="190.5" width="0.1524" layer="91"/>
<label x="200.66" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="UNREG1_1"/>
<wire x1="264.16" y1="190.5" x2="284.48" y2="190.5" width="0.1524" layer="91"/>
<label x="264.16" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="UNREG1_2"/>
<wire x1="304.8" y1="190.5" x2="284.48" y2="190.5" width="0.1524" layer="91"/>
<label x="287.02" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="UNREG1_1"/>
<wire x1="350.52" y1="190.5" x2="370.84" y2="190.5" width="0.1524" layer="91"/>
<label x="350.52" y="190.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="UNREG1_1"/>
<wire x1="93.98" y1="83.82" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<label x="93.98" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="UNREG1_2"/>
<wire x1="48.26" y1="83.82" x2="30.48" y2="83.82" width="0.1524" layer="91"/>
<label x="33.02" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="UNREG1_2"/>
<wire x1="134.62" y1="83.82" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<label x="116.84" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="UNREG1_1"/>
<wire x1="180.34" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<label x="182.88" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3_2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="3V3_2_2"/>
<wire x1="48.26" y1="187.96" x2="30.48" y2="187.96" width="0.1524" layer="91"/>
<label x="33.02" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="3V3_2_1"/>
<wire x1="93.98" y1="187.96" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<label x="93.98" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="3V3_2_1"/>
<wire x1="177.8" y1="187.96" x2="198.12" y2="187.96" width="0.1524" layer="91"/>
<label x="180.34" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="3V3_2_2"/>
<wire x1="132.08" y1="187.96" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<label x="116.84" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="3V3_2_2"/>
<wire x1="218.44" y1="187.96" x2="198.12" y2="187.96" width="0.1524" layer="91"/>
<label x="200.66" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="3V3_2_1"/>
<wire x1="264.16" y1="187.96" x2="284.48" y2="187.96" width="0.1524" layer="91"/>
<label x="264.16" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="3V3_2_2"/>
<wire x1="304.8" y1="187.96" x2="284.48" y2="187.96" width="0.1524" layer="91"/>
<label x="287.02" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="3V3_2_1"/>
<wire x1="350.52" y1="187.96" x2="370.84" y2="187.96" width="0.1524" layer="91"/>
<label x="350.52" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="3V3_2_1"/>
<wire x1="93.98" y1="81.28" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<label x="93.98" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="3V3_2_2"/>
<wire x1="48.26" y1="81.28" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
<label x="33.02" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="3V3_2_2"/>
<wire x1="134.62" y1="81.28" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<label x="116.84" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="3V3_2_1"/>
<wire x1="180.34" y1="81.28" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<label x="182.88" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="TEMP_1(+X)" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="TEMP_1(+X)"/>
<wire x1="48.26" y1="185.42" x2="30.48" y2="185.42" width="0.1524" layer="91"/>
<label x="30.48" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="RAW_PWR" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="RAW_POWER_2"/>
<wire x1="48.26" y1="182.88" x2="30.48" y2="182.88" width="0.1524" layer="91"/>
<label x="33.02" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="RAW_POWER_1"/>
<wire x1="93.98" y1="182.88" x2="114.3" y2="182.88" width="0.1524" layer="91"/>
<label x="93.98" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="RAW_POWER_1"/>
<wire x1="177.8" y1="182.88" x2="198.12" y2="182.88" width="0.1524" layer="91"/>
<label x="180.34" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="RAW_POWER_2"/>
<wire x1="132.08" y1="182.88" x2="114.3" y2="182.88" width="0.1524" layer="91"/>
<label x="116.84" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRSC_-Y" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="POWERSC_-Y"/>
<wire x1="93.98" y1="193.04" x2="114.3" y2="193.04" width="0.1524" layer="91"/>
<label x="93.98" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRSC_-Z" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="POWERSRC_-Z"/>
<wire x1="93.98" y1="177.8" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
<label x="93.98" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRSC_-X" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="POWERSC_-X"/>
<wire x1="93.98" y1="180.34" x2="114.3" y2="180.34" width="0.1524" layer="91"/>
<label x="93.98" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD3" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD3"/>
<wire x1="48.26" y1="165.1" x2="30.48" y2="165.1" width="0.1524" layer="91"/>
<label x="30.48" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="266.7" y1="116.84" x2="274.32" y2="116.84" width="0.1524" layer="91"/>
<label x="274.32" y="116.84" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CPLD5" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD5"/>
<wire x1="48.26" y1="162.56" x2="30.48" y2="162.56" width="0.1524" layer="91"/>
<label x="30.48" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="266.7" y1="114.3" x2="274.32" y2="114.3" width="0.1524" layer="91"/>
<label x="274.32" y="114.3" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CPLD7" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD7"/>
<wire x1="48.26" y1="160.02" x2="30.48" y2="160.02" width="0.1524" layer="91"/>
<label x="30.48" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="259.08" y1="111.76" x2="251.46" y2="111.76" width="0.1524" layer="91"/>
<label x="251.46" y="111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CPLD4" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD4"/>
<wire x1="93.98" y1="162.56" x2="114.3" y2="162.56" width="0.1524" layer="91"/>
<label x="93.98" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="259.08" y1="116.84" x2="251.46" y2="116.84" width="0.1524" layer="91"/>
<label x="251.46" y="116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CPLD6" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="CPLD6"/>
<wire x1="93.98" y1="160.02" x2="114.3" y2="160.02" width="0.1524" layer="91"/>
<label x="93.98" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="259.08" y1="114.3" x2="251.46" y2="114.3" width="0.1524" layer="91"/>
<label x="251.46" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TEMP_5(-X)" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="TEMP_5(-X)"/>
<wire x1="48.26" y1="180.34" x2="30.48" y2="180.34" width="0.1524" layer="91"/>
<label x="30.48" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="TEMP_3(-Z)" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="TEMP_3(-Z)"/>
<wire x1="48.26" y1="177.8" x2="30.48" y2="177.8" width="0.1524" layer="91"/>
<label x="30.48" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="TEMP_4(+Y)" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="TEMP_4(+Y)"/>
<wire x1="48.26" y1="172.72" x2="30.48" y2="172.72" width="0.1524" layer="91"/>
<label x="30.48" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="UNREG2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="UNREG_2_2"/>
<wire x1="48.26" y1="175.26" x2="30.48" y2="175.26" width="0.1524" layer="91"/>
<label x="33.02" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="UNREG_2_1"/>
<wire x1="93.98" y1="175.26" x2="114.3" y2="175.26" width="0.1524" layer="91"/>
<label x="93.98" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="UNREG_2_1"/>
<wire x1="177.8" y1="175.26" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<label x="180.34" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="UNREG_2_2"/>
<wire x1="132.08" y1="175.26" x2="114.3" y2="175.26" width="0.1524" layer="91"/>
<label x="114.3" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="UNREG_2_2"/>
<wire x1="218.44" y1="175.26" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<label x="200.66" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="UNREG_2_1"/>
<wire x1="264.16" y1="175.26" x2="284.48" y2="175.26" width="0.1524" layer="91"/>
<label x="264.16" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="UNREG_2_2"/>
<wire x1="304.8" y1="175.26" x2="284.48" y2="175.26" width="0.1524" layer="91"/>
<label x="287.02" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="UNREG_2_1"/>
<wire x1="350.52" y1="175.26" x2="370.84" y2="175.26" width="0.1524" layer="91"/>
<label x="350.52" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="UNREG_2_1"/>
<wire x1="93.98" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<label x="93.98" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="UNREG_2_2"/>
<wire x1="48.26" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<label x="33.02" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="UNREG_2_2"/>
<wire x1="134.62" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<label x="116.84" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="UNREG_2_1"/>
<wire x1="180.34" y1="68.58" x2="200.66" y2="68.58" width="0.1524" layer="91"/>
<label x="182.88" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3_1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="3V3_1_2"/>
<wire x1="48.26" y1="157.48" x2="30.48" y2="157.48" width="0.1524" layer="91"/>
<label x="33.02" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="FAB" gate="G$1" pin="3V3_1_1"/>
<wire x1="93.98" y1="157.48" x2="114.3" y2="157.48" width="0.1524" layer="91"/>
<label x="93.98" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="3V3_1_1"/>
<wire x1="177.8" y1="157.48" x2="198.12" y2="157.48" width="0.1524" layer="91"/>
<label x="180.34" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="3V3_1_2"/>
<wire x1="132.08" y1="157.48" x2="114.3" y2="157.48" width="0.1524" layer="91"/>
<label x="116.84" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="3V3_1_1"/>
<wire x1="264.16" y1="157.48" x2="284.48" y2="157.48" width="0.1524" layer="91"/>
<label x="264.16" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="3V3_1_2"/>
<wire x1="218.44" y1="157.48" x2="198.12" y2="157.48" width="0.1524" layer="91"/>
<label x="200.66" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="3V3_1_2"/>
<wire x1="304.8" y1="157.48" x2="284.48" y2="157.48" width="0.1524" layer="91"/>
<label x="287.02" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_I" gate="G$1" pin="3V3_1_1"/>
<wire x1="350.52" y1="157.48" x2="370.84" y2="157.48" width="0.1524" layer="91"/>
<label x="350.52" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="3V3_1_1"/>
<wire x1="93.98" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<label x="93.98" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="3V3_1_2"/>
<wire x1="48.26" y1="50.8" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<label x="33.02" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="3V3_1_2"/>
<wire x1="134.62" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<label x="116.84" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="3V3_1_1"/>
<wire x1="180.34" y1="50.8" x2="200.66" y2="50.8" width="0.1524" layer="91"/>
<label x="182.88" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="KILL_SW" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="KILL_SW1"/>
<wire x1="93.98" y1="170.18" x2="114.3" y2="170.18" width="0.1524" layer="91"/>
<label x="93.98" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="KILL_SW1"/>
<wire x1="177.8" y1="170.18" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<label x="180.34" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD8" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD8"/>
<wire x1="177.8" y1="193.04" x2="198.12" y2="193.04" width="0.1524" layer="91"/>
<label x="180.34" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<label x="251.46" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CPLD9" class="0">
<segment>
<wire x1="274.32" y1="99.06" x2="266.7" y2="99.06" width="0.1524" layer="91"/>
<label x="274.32" y="99.06" size="1.778" layer="95" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="CLPD9"/>
<wire x1="132.08" y1="193.04" x2="114.3" y2="193.04" width="0.1524" layer="91"/>
<label x="114.3" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD10" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD10"/>
<wire x1="177.8" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<label x="180.34" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="96.52" x2="251.46" y2="96.52" width="0.1524" layer="91"/>
<label x="251.46" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="CPLD11" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD11"/>
<wire x1="132.08" y1="185.42" x2="114.3" y2="185.42" width="0.1524" layer="91"/>
<label x="114.3" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="96.52" x2="274.32" y2="96.52" width="0.1524" layer="91"/>
<label x="274.32" y="96.52" size="1.778" layer="95" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="4"/>
</segment>
</net>
<net name="CPLD12" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD12"/>
<wire x1="177.8" y1="180.34" x2="198.12" y2="180.34" width="0.1524" layer="91"/>
<label x="180.34" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="93.98" x2="251.46" y2="93.98" width="0.1524" layer="91"/>
<label x="251.46" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="5"/>
</segment>
</net>
<net name="CPLD13" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD13"/>
<wire x1="132.08" y1="180.34" x2="114.3" y2="180.34" width="0.1524" layer="91"/>
<label x="114.3" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="93.98" x2="274.32" y2="93.98" width="0.1524" layer="91"/>
<label x="274.32" y="93.98" size="1.778" layer="95" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="6"/>
</segment>
</net>
<net name="CPLD14" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD14"/>
<wire x1="177.8" y1="177.8" x2="198.12" y2="177.8" width="0.1524" layer="91"/>
<label x="180.34" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="91.44" x2="251.46" y2="91.44" width="0.1524" layer="91"/>
<label x="251.46" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="7"/>
</segment>
</net>
<net name="CPLD15" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD15"/>
<wire x1="132.08" y1="177.8" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
<label x="114.3" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="91.44" x2="274.32" y2="91.44" width="0.1524" layer="91"/>
<label x="274.32" y="91.44" size="1.778" layer="95" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="8"/>
</segment>
</net>
<net name="CPLD16" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD16"/>
<wire x1="177.8" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<label x="180.34" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="88.9" x2="251.46" y2="88.9" width="0.1524" layer="91"/>
<label x="251.46" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="9"/>
</segment>
</net>
<net name="CPLD17" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD17"/>
<wire x1="132.08" y1="172.72" x2="114.3" y2="172.72" width="0.1524" layer="91"/>
<label x="114.3" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="88.9" x2="274.32" y2="88.9" width="0.1524" layer="91"/>
<label x="274.32" y="88.9" size="1.778" layer="95" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="10"/>
</segment>
</net>
<net name="CPLD18" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="CPLD18"/>
<wire x1="132.08" y1="167.64" x2="114.3" y2="167.64" width="0.1524" layer="91"/>
<label x="114.3" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="86.36" x2="251.46" y2="86.36" width="0.1524" layer="91"/>
<label x="251.46" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="11"/>
</segment>
</net>
<net name="OBC_COM2" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM2"/>
<wire x1="132.08" y1="210.82" x2="114.3" y2="210.82" width="0.1524" layer="91"/>
<label x="114.3" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM2"/>
<wire x1="218.44" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<label x="198.12" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM3" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM3"/>
<wire x1="177.8" y1="165.1" x2="198.12" y2="165.1" width="0.1524" layer="91"/>
<label x="180.34" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM3"/>
<wire x1="264.16" y1="165.1" x2="284.48" y2="165.1" width="0.1524" layer="91"/>
<label x="264.16" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM4" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM4"/>
<wire x1="132.08" y1="165.1" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<label x="114.3" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM4"/>
<wire x1="218.44" y1="165.1" x2="198.12" y2="165.1" width="0.1524" layer="91"/>
<label x="198.12" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM5" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM5"/>
<wire x1="177.8" y1="162.56" x2="198.12" y2="162.56" width="0.1524" layer="91"/>
<label x="180.34" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM5"/>
<wire x1="264.16" y1="162.56" x2="284.48" y2="162.56" width="0.1524" layer="91"/>
<label x="264.16" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM6" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM6"/>
<wire x1="132.08" y1="162.56" x2="114.3" y2="162.56" width="0.1524" layer="91"/>
<label x="114.3" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM6"/>
<wire x1="218.44" y1="162.56" x2="198.12" y2="162.56" width="0.1524" layer="91"/>
<label x="198.12" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM7" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM7"/>
<wire x1="177.8" y1="160.02" x2="198.12" y2="160.02" width="0.1524" layer="91"/>
<label x="180.34" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM7"/>
<wire x1="264.16" y1="160.02" x2="284.48" y2="160.02" width="0.1524" layer="91"/>
<label x="264.16" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="OBC_COM8" class="0">
<segment>
<pinref part="OBC" gate="G$1" pin="OBC_COM8"/>
<wire x1="132.08" y1="160.02" x2="114.3" y2="160.02" width="0.1524" layer="91"/>
<label x="114.3" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="COM" gate="G$1" pin="OBC_COM8"/>
<wire x1="218.44" y1="160.02" x2="198.12" y2="160.02" width="0.1524" layer="91"/>
<label x="198.12" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD20" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD20"/>
<wire x1="218.44" y1="198.12" x2="198.12" y2="198.12" width="0.1524" layer="91"/>
<label x="198.12" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="274.32" y1="73.66" x2="266.7" y2="73.66" width="0.1524" layer="91"/>
<label x="274.32" y="73.66" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CPLD22" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD22"/>
<wire x1="218.44" y1="195.58" x2="198.12" y2="195.58" width="0.1524" layer="91"/>
<label x="198.12" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="71.12" x2="274.32" y2="71.12" width="0.1524" layer="91"/>
<label x="274.32" y="71.12" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="4"/>
</segment>
</net>
<net name="CPLD24" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD24"/>
<wire x1="218.44" y1="193.04" x2="198.12" y2="193.04" width="0.1524" layer="91"/>
<label x="198.12" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="68.58" x2="274.32" y2="68.58" width="0.1524" layer="91"/>
<label x="274.32" y="68.58" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="6"/>
</segment>
</net>
<net name="CPLD26" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD26"/>
<wire x1="218.44" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<label x="198.12" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="66.04" x2="274.32" y2="66.04" width="0.1524" layer="91"/>
<label x="274.32" y="66.04" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="8"/>
</segment>
</net>
<net name="CPLD28" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD28"/>
<wire x1="218.44" y1="182.88" x2="198.12" y2="182.88" width="0.1524" layer="91"/>
<label x="198.12" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="63.5" x2="274.32" y2="63.5" width="0.1524" layer="91"/>
<label x="274.32" y="63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="10"/>
</segment>
</net>
<net name="CPLD30" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD30"/>
<wire x1="218.44" y1="180.34" x2="198.12" y2="180.34" width="0.1524" layer="91"/>
<label x="198.12" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="274.32" y1="60.96" x2="266.7" y2="60.96" width="0.1524" layer="91"/>
<label x="274.32" y="60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="12"/>
</segment>
</net>
<net name="CPLD32" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD32"/>
<wire x1="218.44" y1="177.8" x2="198.12" y2="177.8" width="0.1524" layer="91"/>
<label x="198.12" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="58.42" x2="274.32" y2="58.42" width="0.1524" layer="91"/>
<label x="274.32" y="58.42" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="14"/>
</segment>
</net>
<net name="CPLD34" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD34"/>
<wire x1="218.44" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<label x="198.12" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="55.88" x2="274.32" y2="55.88" width="0.1524" layer="91"/>
<label x="274.32" y="55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="16"/>
</segment>
</net>
<net name="CPLD36" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD36"/>
<wire x1="218.44" y1="170.18" x2="198.12" y2="170.18" width="0.1524" layer="91"/>
<label x="198.12" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="53.34" x2="274.32" y2="53.34" width="0.1524" layer="91"/>
<label x="274.32" y="53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="18"/>
</segment>
</net>
<net name="CPLD38" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD38"/>
<wire x1="218.44" y1="167.64" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<label x="198.12" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="266.7" y1="50.8" x2="274.32" y2="50.8" width="0.1524" layer="91"/>
<label x="274.32" y="50.8" size="1.778" layer="95" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="20"/>
</segment>
</net>
<net name="COM_RAB_GIO5" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="COM_RAB_GIO5"/>
<wire x1="264.16" y1="213.36" x2="284.48" y2="213.36" width="0.1524" layer="91"/>
<label x="264.16" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD19" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD19"/>
<wire x1="264.16" y1="198.12" x2="284.48" y2="198.12" width="0.1524" layer="91"/>
<label x="264.16" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="73.66" x2="251.46" y2="73.66" width="0.1524" layer="91"/>
<label x="251.46" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CPLD21" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD21"/>
<wire x1="264.16" y1="195.58" x2="284.48" y2="195.58" width="0.1524" layer="91"/>
<label x="264.16" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="71.12" x2="251.46" y2="71.12" width="0.1524" layer="91"/>
<label x="251.46" y="71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="CPLD23" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD23"/>
<wire x1="264.16" y1="193.04" x2="284.48" y2="193.04" width="0.1524" layer="91"/>
<label x="264.16" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="68.58" x2="251.46" y2="68.58" width="0.1524" layer="91"/>
<label x="251.46" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="5"/>
</segment>
</net>
<net name="CPLD25" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD25"/>
<wire x1="264.16" y1="185.42" x2="284.48" y2="185.42" width="0.1524" layer="91"/>
<label x="264.16" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="66.04" x2="251.46" y2="66.04" width="0.1524" layer="91"/>
<label x="251.46" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="7"/>
</segment>
</net>
<net name="CPLD27" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD27"/>
<wire x1="264.16" y1="182.88" x2="284.48" y2="182.88" width="0.1524" layer="91"/>
<label x="264.16" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="63.5" x2="251.46" y2="63.5" width="0.1524" layer="91"/>
<label x="251.46" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="9"/>
</segment>
</net>
<net name="CPLD29" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD29"/>
<wire x1="264.16" y1="180.34" x2="284.48" y2="180.34" width="0.1524" layer="91"/>
<label x="264.16" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="60.96" x2="251.46" y2="60.96" width="0.1524" layer="91"/>
<label x="251.46" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="11"/>
</segment>
</net>
<net name="CPLD31" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD31"/>
<wire x1="264.16" y1="177.8" x2="284.48" y2="177.8" width="0.1524" layer="91"/>
<label x="264.16" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="58.42" x2="251.46" y2="58.42" width="0.1524" layer="91"/>
<label x="251.46" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="13"/>
</segment>
</net>
<net name="CPLD33" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD33"/>
<wire x1="264.16" y1="172.72" x2="284.48" y2="172.72" width="0.1524" layer="91"/>
<label x="264.16" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="55.88" x2="251.46" y2="55.88" width="0.1524" layer="91"/>
<label x="251.46" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="15"/>
</segment>
</net>
<net name="CPLD35" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD35"/>
<wire x1="264.16" y1="170.18" x2="284.48" y2="170.18" width="0.1524" layer="91"/>
<label x="264.16" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="53.34" x2="251.46" y2="53.34" width="0.1524" layer="91"/>
<label x="251.46" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="17"/>
</segment>
</net>
<net name="CPLD37" class="0">
<segment>
<pinref part="COM" gate="G$1" pin="CPLD37"/>
<wire x1="264.16" y1="167.64" x2="284.48" y2="167.64" width="0.1524" layer="91"/>
<label x="264.16" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="259.08" y1="50.8" x2="251.46" y2="50.8" width="0.1524" layer="91"/>
<label x="251.46" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="19"/>
</segment>
</net>
<net name="CPLD44" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD44"/>
<wire x1="304.8" y1="193.04" x2="284.48" y2="193.04" width="0.1524" layer="91"/>
<label x="284.48" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD44"/>
<wire x1="48.26" y1="86.36" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<label x="30.48" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="119.38" x2="325.12" y2="119.38" width="0.1524" layer="91"/>
<label x="332.74" y="119.38" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CPLD46" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD46"/>
<wire x1="304.8" y1="185.42" x2="284.48" y2="185.42" width="0.1524" layer="91"/>
<label x="284.48" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD46"/>
<wire x1="48.26" y1="78.74" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<label x="30.48" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="116.84" x2="332.74" y2="116.84" width="0.1524" layer="91"/>
<label x="332.74" y="116.84" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="4"/>
</segment>
</net>
<net name="CPLD48" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD48"/>
<wire x1="304.8" y1="182.88" x2="284.48" y2="182.88" width="0.1524" layer="91"/>
<label x="284.48" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD48"/>
<wire x1="48.26" y1="76.2" x2="30.48" y2="76.2" width="0.1524" layer="91"/>
<label x="30.48" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="114.3" x2="332.74" y2="114.3" width="0.1524" layer="91"/>
<label x="332.74" y="114.3" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="6"/>
</segment>
</net>
<net name="CPLD50" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD50"/>
<wire x1="304.8" y1="180.34" x2="284.48" y2="180.34" width="0.1524" layer="91"/>
<label x="284.48" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD50"/>
<wire x1="48.26" y1="73.66" x2="30.48" y2="73.66" width="0.1524" layer="91"/>
<label x="30.48" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="111.76" x2="332.74" y2="111.76" width="0.1524" layer="91"/>
<label x="332.74" y="111.76" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="8"/>
</segment>
</net>
<net name="CPLD52" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD52"/>
<wire x1="304.8" y1="177.8" x2="284.48" y2="177.8" width="0.1524" layer="91"/>
<label x="284.48" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD52"/>
<wire x1="48.26" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<label x="30.48" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="109.22" x2="332.74" y2="109.22" width="0.1524" layer="91"/>
<label x="332.74" y="109.22" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="10"/>
</segment>
</net>
<net name="CPLD54" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD54"/>
<wire x1="304.8" y1="172.72" x2="284.48" y2="172.72" width="0.1524" layer="91"/>
<label x="284.48" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD54"/>
<wire x1="48.26" y1="66.04" x2="30.48" y2="66.04" width="0.1524" layer="91"/>
<label x="30.48" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="106.68" x2="325.12" y2="106.68" width="0.1524" layer="91"/>
<label x="332.74" y="106.68" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="12"/>
</segment>
</net>
<net name="CPLD56" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD56"/>
<wire x1="304.8" y1="170.18" x2="284.48" y2="170.18" width="0.1524" layer="91"/>
<label x="284.48" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD56"/>
<wire x1="48.26" y1="63.5" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<label x="30.48" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="104.14" x2="332.74" y2="104.14" width="0.1524" layer="91"/>
<label x="332.74" y="104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="14"/>
</segment>
</net>
<net name="CPLD58" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD58"/>
<wire x1="304.8" y1="167.64" x2="284.48" y2="167.64" width="0.1524" layer="91"/>
<label x="284.48" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD58"/>
<wire x1="48.26" y1="60.96" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<label x="30.48" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="101.6" x2="332.74" y2="101.6" width="0.1524" layer="91"/>
<label x="332.74" y="101.6" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="16"/>
</segment>
</net>
<net name="CPLD60" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD60"/>
<wire x1="304.8" y1="165.1" x2="284.48" y2="165.1" width="0.1524" layer="91"/>
<label x="284.48" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD60"/>
<wire x1="48.26" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<label x="30.48" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="99.06" x2="332.74" y2="99.06" width="0.1524" layer="91"/>
<label x="332.74" y="99.06" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="18"/>
</segment>
</net>
<net name="CPLD62" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD62"/>
<wire x1="304.8" y1="162.56" x2="284.48" y2="162.56" width="0.1524" layer="91"/>
<label x="284.48" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD62"/>
<wire x1="48.26" y1="55.88" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<label x="30.48" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="96.52" x2="332.74" y2="96.52" width="0.1524" layer="91"/>
<label x="332.74" y="96.52" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="20"/>
</segment>
</net>
<net name="CPLD64" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD64"/>
<wire x1="304.8" y1="160.02" x2="284.48" y2="160.02" width="0.1524" layer="91"/>
<label x="284.48" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD64"/>
<wire x1="48.26" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<label x="30.48" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="93.98" x2="332.74" y2="93.98" width="0.1524" layer="91"/>
<label x="332.74" y="93.98" size="1.778" layer="95" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="22"/>
</segment>
</net>
<net name="CPLD43" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD43"/>
<wire x1="350.52" y1="193.04" x2="370.84" y2="193.04" width="0.1524" layer="91"/>
<label x="350.52" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD43"/>
<wire x1="93.98" y1="86.36" x2="114.3" y2="86.36" width="0.1524" layer="91"/>
<label x="93.98" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="119.38" x2="309.88" y2="119.38" width="0.1524" layer="91"/>
<label x="309.88" y="119.38" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CPLD45" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD45"/>
<wire x1="350.52" y1="185.42" x2="370.84" y2="185.42" width="0.1524" layer="91"/>
<label x="350.52" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD45"/>
<wire x1="93.98" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<label x="93.98" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="116.84" x2="309.88" y2="116.84" width="0.1524" layer="91"/>
<label x="309.88" y="116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="3"/>
</segment>
</net>
<net name="CPLD47" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD47"/>
<wire x1="350.52" y1="182.88" x2="370.84" y2="182.88" width="0.1524" layer="91"/>
<label x="350.52" y="182.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD47"/>
<wire x1="93.98" y1="76.2" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<label x="93.98" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="114.3" x2="309.88" y2="114.3" width="0.1524" layer="91"/>
<label x="309.88" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="5"/>
</segment>
</net>
<net name="CPLD49" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD49"/>
<wire x1="350.52" y1="180.34" x2="370.84" y2="180.34" width="0.1524" layer="91"/>
<label x="350.52" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD49"/>
<wire x1="93.98" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<label x="93.98" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="111.76" x2="309.88" y2="111.76" width="0.1524" layer="91"/>
<label x="309.88" y="111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="7"/>
</segment>
</net>
<net name="CPLD51" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD51"/>
<wire x1="350.52" y1="177.8" x2="370.84" y2="177.8" width="0.1524" layer="91"/>
<label x="350.52" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD51"/>
<wire x1="93.98" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
<label x="93.98" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="109.22" x2="309.88" y2="109.22" width="0.1524" layer="91"/>
<label x="309.88" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="9"/>
</segment>
</net>
<net name="CPLD53" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD53"/>
<wire x1="350.52" y1="172.72" x2="370.84" y2="172.72" width="0.1524" layer="91"/>
<label x="350.52" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD53"/>
<wire x1="93.98" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<label x="93.98" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="106.68" x2="309.88" y2="106.68" width="0.1524" layer="91"/>
<label x="309.88" y="106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="11"/>
</segment>
</net>
<net name="CPLD55" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD55"/>
<wire x1="350.52" y1="170.18" x2="370.84" y2="170.18" width="0.1524" layer="91"/>
<label x="350.52" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD55"/>
<wire x1="93.98" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<label x="93.98" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="104.14" x2="309.88" y2="104.14" width="0.1524" layer="91"/>
<label x="309.88" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="13"/>
</segment>
</net>
<net name="CPLD57" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD57"/>
<wire x1="350.52" y1="167.64" x2="370.84" y2="167.64" width="0.1524" layer="91"/>
<label x="350.52" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD57"/>
<wire x1="93.98" y1="60.96" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
<label x="93.98" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="101.6" x2="309.88" y2="101.6" width="0.1524" layer="91"/>
<label x="309.88" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="15"/>
</segment>
</net>
<net name="CPLD59" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD59"/>
<wire x1="350.52" y1="165.1" x2="370.84" y2="165.1" width="0.1524" layer="91"/>
<label x="350.52" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD59"/>
<wire x1="93.98" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<label x="93.98" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="99.06" x2="309.88" y2="99.06" width="0.1524" layer="91"/>
<label x="309.88" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="17"/>
</segment>
</net>
<net name="CPLD61" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD61"/>
<wire x1="350.52" y1="162.56" x2="370.84" y2="162.56" width="0.1524" layer="91"/>
<label x="350.52" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD61"/>
<wire x1="93.98" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<label x="93.98" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="96.52" x2="309.88" y2="96.52" width="0.1524" layer="91"/>
<label x="309.88" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="19"/>
</segment>
</net>
<net name="CPLD63" class="0">
<segment>
<pinref part="MSN_I" gate="G$1" pin="CPLD63"/>
<wire x1="350.52" y1="160.02" x2="370.84" y2="160.02" width="0.1524" layer="91"/>
<label x="350.52" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MSN_II" gate="G$1" pin="CPLD63"/>
<wire x1="93.98" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<label x="93.98" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="93.98" x2="309.88" y2="93.98" width="0.1524" layer="91"/>
<label x="309.88" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP4" gate="G$1" pin="21"/>
</segment>
</net>
<net name="MTQ_RTN_-Z" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_RTN_-Z"/>
<wire x1="48.26" y1="91.44" x2="30.48" y2="91.44" width="0.1524" layer="91"/>
<label x="30.48" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MTQ_RTN_-Y" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_RTN_-Y"/>
<wire x1="48.26" y1="88.9" x2="30.48" y2="88.9" width="0.1524" layer="91"/>
<label x="30.48" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD66" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD66"/>
<wire x1="134.62" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="114.3" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="78.74" x2="325.12" y2="78.74" width="0.1524" layer="91"/>
<label x="332.74" y="78.74" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CPLD68" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD68"/>
<wire x1="134.62" y1="88.9" x2="114.3" y2="88.9" width="0.1524" layer="91"/>
<label x="114.3" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="76.2" x2="332.74" y2="76.2" width="0.1524" layer="91"/>
<label x="332.74" y="76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="4"/>
</segment>
</net>
<net name="CPLD70" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD70"/>
<wire x1="134.62" y1="86.36" x2="114.3" y2="86.36" width="0.1524" layer="91"/>
<label x="114.3" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="73.66" x2="332.74" y2="73.66" width="0.1524" layer="91"/>
<label x="332.74" y="73.66" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="6"/>
</segment>
</net>
<net name="CPLD72" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD72"/>
<wire x1="134.62" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<label x="114.3" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="71.12" x2="332.74" y2="71.12" width="0.1524" layer="91"/>
<label x="332.74" y="71.12" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="8"/>
</segment>
</net>
<net name="CPLD74" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD74"/>
<wire x1="134.62" y1="76.2" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<label x="114.3" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="68.58" x2="332.74" y2="68.58" width="0.1524" layer="91"/>
<label x="332.74" y="68.58" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="10"/>
</segment>
</net>
<net name="CPLD76" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD76"/>
<wire x1="134.62" y1="73.66" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<label x="114.3" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="66.04" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<label x="332.74" y="66.04" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="12"/>
</segment>
</net>
<net name="CPLD78" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD78"/>
<wire x1="134.62" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
<label x="114.3" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="63.5" x2="332.74" y2="63.5" width="0.1524" layer="91"/>
<label x="332.74" y="63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="14"/>
</segment>
</net>
<net name="CPLD80" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD80"/>
<wire x1="134.62" y1="66.04" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<label x="114.3" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="60.96" x2="332.74" y2="60.96" width="0.1524" layer="91"/>
<label x="332.74" y="60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="16"/>
</segment>
</net>
<net name="CPLD82" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD82"/>
<wire x1="134.62" y1="63.5" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<label x="114.3" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="58.42" x2="332.74" y2="58.42" width="0.1524" layer="91"/>
<label x="332.74" y="58.42" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="18"/>
</segment>
</net>
<net name="CPLD84" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD84"/>
<wire x1="134.62" y1="60.96" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
<label x="114.3" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="55.88" x2="332.74" y2="55.88" width="0.1524" layer="91"/>
<label x="332.74" y="55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="20"/>
</segment>
</net>
<net name="CPLD86" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD86"/>
<wire x1="134.62" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<label x="114.3" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="53.34" x2="332.74" y2="53.34" width="0.1524" layer="91"/>
<label x="332.74" y="53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="22"/>
</segment>
</net>
<net name="CPLD88" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD88"/>
<wire x1="134.62" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="50.8" x2="332.74" y2="50.8" width="0.1524" layer="91"/>
<label x="332.74" y="50.8" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="24"/>
</segment>
</net>
<net name="CPLD90" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD90"/>
<wire x1="134.62" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<label x="114.3" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="325.12" y1="48.26" x2="332.74" y2="48.26" width="0.1524" layer="91"/>
<label x="332.74" y="48.26" size="1.778" layer="95" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="26"/>
</segment>
</net>
<net name="CPLD65" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD65"/>
<wire x1="180.34" y1="91.44" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<label x="180.34" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="78.74" x2="309.88" y2="78.74" width="0.1524" layer="91"/>
<label x="309.88" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CPLD67" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD67"/>
<wire x1="180.34" y1="88.9" x2="200.66" y2="88.9" width="0.1524" layer="91"/>
<label x="180.34" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="76.2" x2="309.88" y2="76.2" width="0.1524" layer="91"/>
<label x="309.88" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="3"/>
</segment>
</net>
<net name="CPLD69" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD69"/>
<wire x1="180.34" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<label x="180.34" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="73.66" x2="309.88" y2="73.66" width="0.1524" layer="91"/>
<label x="309.88" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="CPLD71" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD71"/>
<wire x1="180.34" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<label x="180.34" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="71.12" x2="309.88" y2="71.12" width="0.1524" layer="91"/>
<label x="309.88" y="71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="7"/>
</segment>
</net>
<net name="CPLD73" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD73"/>
<wire x1="180.34" y1="76.2" x2="200.66" y2="76.2" width="0.1524" layer="91"/>
<label x="180.34" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="68.58" x2="309.88" y2="68.58" width="0.1524" layer="91"/>
<label x="309.88" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="9"/>
</segment>
</net>
<net name="CPLD75" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD75"/>
<wire x1="180.34" y1="73.66" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<label x="180.34" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="66.04" x2="309.88" y2="66.04" width="0.1524" layer="91"/>
<label x="309.88" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="11"/>
</segment>
</net>
<net name="CPLD77" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD77"/>
<wire x1="180.34" y1="71.12" x2="200.66" y2="71.12" width="0.1524" layer="91"/>
<label x="180.34" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="63.5" x2="309.88" y2="63.5" width="0.1524" layer="91"/>
<label x="309.88" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="13"/>
</segment>
</net>
<net name="CPLD79" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD79"/>
<wire x1="180.34" y1="66.04" x2="200.66" y2="66.04" width="0.1524" layer="91"/>
<label x="180.34" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="60.96" x2="309.88" y2="60.96" width="0.1524" layer="91"/>
<label x="309.88" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="15"/>
</segment>
</net>
<net name="CPLD81" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD81"/>
<wire x1="180.34" y1="63.5" x2="200.66" y2="63.5" width="0.1524" layer="91"/>
<label x="180.34" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="58.42" x2="309.88" y2="58.42" width="0.1524" layer="91"/>
<label x="309.88" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="17"/>
</segment>
</net>
<net name="CPLD83" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD83"/>
<wire x1="180.34" y1="60.96" x2="200.66" y2="60.96" width="0.1524" layer="91"/>
<label x="180.34" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="55.88" x2="309.88" y2="55.88" width="0.1524" layer="91"/>
<label x="309.88" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="19"/>
</segment>
</net>
<net name="CPLD87" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD87"/>
<wire x1="180.34" y1="55.88" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<label x="180.34" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="50.8" x2="309.88" y2="50.8" width="0.1524" layer="91"/>
<label x="309.88" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="23"/>
</segment>
</net>
<net name="CPLD89" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD89"/>
<wire x1="180.34" y1="53.34" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<label x="180.34" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="48.26" x2="309.88" y2="48.26" width="0.1524" layer="91"/>
<label x="309.88" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="25"/>
</segment>
</net>
<net name="DEBUG1" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG1"/>
<wire x1="93.98" y1="218.44" x2="114.3" y2="218.44" width="0.1524" layer="91"/>
<label x="93.98" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG1"/>
<wire x1="177.8" y1="218.44" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<label x="177.8" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEBUG2" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG2"/>
<wire x1="48.26" y1="218.44" x2="30.48" y2="218.44" width="0.1524" layer="91"/>
<label x="30.48" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG2"/>
<wire x1="132.08" y1="218.44" x2="114.3" y2="218.44" width="0.1524" layer="91"/>
<label x="114.3" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEBUG3" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG3"/>
<wire x1="93.98" y1="215.9" x2="114.3" y2="215.9" width="0.1524" layer="91"/>
<label x="93.98" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG3"/>
<wire x1="177.8" y1="215.9" x2="198.12" y2="215.9" width="0.1524" layer="91"/>
<label x="177.8" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEBUG4" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG4"/>
<wire x1="48.26" y1="215.9" x2="30.48" y2="215.9" width="0.1524" layer="91"/>
<label x="30.48" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG4"/>
<wire x1="132.08" y1="215.9" x2="114.3" y2="215.9" width="0.1524" layer="91"/>
<label x="114.3" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEBUG5" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG5"/>
<wire x1="93.98" y1="213.36" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
<label x="93.98" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG5"/>
<wire x1="177.8" y1="213.36" x2="198.12" y2="213.36" width="0.1524" layer="91"/>
<label x="177.8" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="DEBUG6" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="DEBUG6"/>
<wire x1="48.26" y1="213.36" x2="30.48" y2="213.36" width="0.1524" layer="91"/>
<label x="30.48" y="213.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="OBC" gate="G$1" pin="DEBUG6"/>
<wire x1="132.08" y1="213.36" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
<label x="114.3" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="MTQ_PWR_-Z" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_PWR_-Z"/>
<wire x1="93.98" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="93.98" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MTQ_PWR_-Y" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_PWR_-Y"/>
<wire x1="93.98" y1="88.9" x2="114.3" y2="88.9" width="0.1524" layer="91"/>
<label x="93.98" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="CPLD85" class="0">
<segment>
<pinref part="RAB" gate="G$1" pin="CPLD85"/>
<wire x1="180.34" y1="58.42" x2="200.66" y2="58.42" width="0.1524" layer="91"/>
<label x="180.34" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="317.5" y1="53.34" x2="309.88" y2="53.34" width="0.1524" layer="91"/>
<label x="309.88" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JP5" gate="G$1" pin="21"/>
</segment>
</net>
<net name="MTQ_PWR_-X" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_PWR_-X"/>
<wire x1="93.98" y1="106.68" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<label x="93.98" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="MTQ_PWR_-X"/>
<wire x1="180.34" y1="106.68" x2="200.66" y2="106.68" width="0.1524" layer="91"/>
<label x="180.34" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="MTQ_RTN_-X" class="0">
<segment>
<pinref part="MSN_II" gate="G$1" pin="MTQ_RTN_-X"/>
<wire x1="48.26" y1="106.68" x2="30.48" y2="106.68" width="0.1524" layer="91"/>
<label x="30.48" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="RAB" gate="G$1" pin="MTQ_RTN_-X"/>
<wire x1="134.62" y1="106.68" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<label x="114.3" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRSC_+X" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="POWERSC_+X"/>
<wire x1="93.98" y1="185.42" x2="114.3" y2="185.42" width="0.1524" layer="91"/>
<label x="93.98" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRSC_+Y" class="0">
<segment>
<pinref part="FAB" gate="G$1" pin="POWERSC_+Y"/>
<wire x1="93.98" y1="172.72" x2="114.3" y2="172.72" width="0.1524" layer="91"/>
<label x="93.98" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="297.18" y="13.97" size="2.54" layer="94">BIRDS-3 TESTBED</text>
<text x="349.25" y="21.59" size="2.54" layer="94">Abhas</text>
<text x="231.14" y="165.1" size="2.54" layer="94">DEPLOYMENT TOGGLE SWITCH</text>
<text x="215.9" y="76.2" size="2.54" layer="94">MTQ CONNECTORS</text>
<text x="142.24" y="106.68" size="2.54" layer="94">POWER PROBES</text>
<text x="152.4" y="165.1" size="2.54" layer="94">RAW POWER INPUT</text>
<text x="88.9" y="165.1" size="2.54" layer="94">RAW INPUT INDICATOR</text>
<text x="228.6" y="114.3" size="2.54" layer="94">POWER SOURCE PROBES</text>
<wire x1="83.82" y1="162.56" x2="134.62" y2="162.56" width="0.254" layer="94"/>
<wire x1="134.62" y1="210.82" x2="83.82" y2="210.82" width="0.254" layer="94"/>
<wire x1="83.82" y1="210.82" x2="83.82" y2="162.56" width="0.254" layer="94"/>
<wire x1="205.74" y1="162.56" x2="205.74" y2="210.82" width="0.254" layer="94"/>
<wire x1="205.74" y1="210.82" x2="134.62" y2="210.82" width="0.254" layer="94"/>
<wire x1="134.62" y1="210.82" x2="134.62" y2="162.56" width="0.254" layer="94"/>
<wire x1="205.74" y1="215.9" x2="205.74" y2="210.82" width="0.254" layer="94"/>
<wire x1="205.74" y1="162.56" x2="299.72" y2="162.56" width="0.254" layer="94"/>
<wire x1="299.72" y1="162.56" x2="299.72" y2="215.9" width="0.254" layer="94"/>
<wire x1="299.72" y1="215.9" x2="205.74" y2="215.9" width="0.254" layer="94"/>
<wire x1="83.82" y1="104.14" x2="83.82" y2="162.56" width="0.254" layer="94"/>
<wire x1="134.62" y1="162.56" x2="195.58" y2="162.56" width="0.254" layer="94"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="104.14" width="0.254" layer="94"/>
<wire x1="165.1" y1="104.14" x2="83.82" y2="104.14" width="0.254" layer="94"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="162.56" width="0.254" layer="94"/>
<wire x1="195.58" y1="162.56" x2="205.74" y2="162.56" width="0.254" layer="94"/>
<wire x1="299.72" y1="162.56" x2="299.72" y2="111.76" width="0.254" layer="94"/>
<wire x1="299.72" y1="111.76" x2="256.54" y2="111.76" width="0.254" layer="94"/>
<wire x1="165.1" y1="104.14" x2="195.58" y2="104.14" width="0.254" layer="94"/>
<wire x1="195.58" y1="73.66" x2="195.58" y2="55.88" width="0.254" layer="94"/>
<wire x1="195.58" y1="55.88" x2="165.1" y2="55.88" width="0.254" layer="94"/>
<wire x1="165.1" y1="55.88" x2="165.1" y2="104.14" width="0.254" layer="94"/>
<wire x1="195.58" y1="111.76" x2="256.54" y2="111.76" width="0.254" layer="94"/>
<wire x1="256.54" y1="111.76" x2="256.54" y2="73.66" width="0.254" layer="94"/>
<wire x1="256.54" y1="73.66" x2="195.58" y2="73.66" width="0.254" layer="94"/>
<wire x1="195.58" y1="73.66" x2="195.58" y2="104.14" width="0.254" layer="94"/>
<text x="167.64" y="58.42" size="2.54" layer="94">3.2 PCB HOLES</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="344.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="344.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="357.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="GND5" gate="1" x="215.9" y="170.18" smashed="yes">
<attribute name="VALUE" x="213.36" y="167.64" size="1.778" layer="96"/>
</instance>
<instance part="STB1" gate="P1" x="172.72" y="187.96" smashed="yes">
<attribute name="NAME" x="182.88" y="187.96" size="1.27" layer="95" align="center"/>
</instance>
<instance part="STB1" gate="P2" x="172.72" y="185.42" smashed="yes">
<attribute name="NAME" x="182.88" y="185.42" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON1" gate="P1" x="233.68" y="106.68" smashed="yes">
<attribute name="NAME" x="243.84" y="106.68" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON1" gate="P2" x="233.68" y="104.14" smashed="yes">
<attribute name="NAME" x="243.84" y="104.14" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON2" gate="P1" x="233.68" y="99.06" smashed="yes">
<attribute name="NAME" x="243.84" y="99.06" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON2" gate="P2" x="233.68" y="96.52" smashed="yes">
<attribute name="NAME" x="243.84" y="96.52" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON3" gate="P1" x="233.68" y="91.44" smashed="yes">
<attribute name="NAME" x="243.84" y="91.44" size="1.27" layer="95" align="center"/>
</instance>
<instance part="CON3" gate="P2" x="233.68" y="88.9" smashed="yes">
<attribute name="NAME" x="243.84" y="88.9" size="1.27" layer="95" align="center"/>
</instance>
<instance part="GND6" gate="1" x="165.1" y="175.26" smashed="yes">
<attribute name="VALUE" x="162.56" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="3V3_1" gate="G$1" x="104.14" y="124.46" smashed="yes" rot="MR90">
<attribute name="NAME" x="99.06" y="121.92" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="3V3_2" gate="G$1" x="104.14" y="116.84" smashed="yes" rot="MR90">
<attribute name="NAME" x="99.06" y="114.3" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="UNREG1" gate="G$1" x="127" y="139.7" smashed="yes" rot="MR90">
<attribute name="NAME" x="121.92" y="137.16" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="UNREG2" gate="G$1" x="127" y="132.08" smashed="yes" rot="MR90">
<attribute name="NAME" x="121.92" y="129.54" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="5V" gate="G$1" x="149.86" y="154.94" smashed="yes" rot="MR90">
<attribute name="NAME" x="144.78" y="152.4" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="RAW_PWR" gate="G$1" x="149.86" y="147.32" smashed="yes" rot="MR90">
<attribute name="NAME" x="144.78" y="144.78" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND7" gate="1" x="93.98" y="109.22" smashed="yes">
<attribute name="VALUE" x="91.44" y="106.68" size="1.778" layer="96"/>
</instance>
<instance part="LED1" gate="G$1" x="96.52" y="187.96" smashed="yes">
<attribute name="NAME" x="100.076" y="183.388" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="GND8" gate="1" x="96.52" y="175.26" smashed="yes">
<attribute name="VALUE" x="93.98" y="172.72" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="109.22" y="195.58" smashed="yes">
<attribute name="NAME" x="105.41" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="105.41" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="P+4" gate="VCC" x="119.38" y="205.74" smashed="yes">
<attribute name="VALUE" x="116.84" y="203.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+X" gate="G$1" x="210.82" y="139.7" smashed="yes" rot="MR90">
<attribute name="NAME" x="205.74" y="137.16" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="-X" gate="G$1" x="210.82" y="132.08" smashed="yes" rot="MR90">
<attribute name="NAME" x="205.74" y="129.54" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="+Y" gate="G$1" x="233.68" y="154.94" smashed="yes" rot="MR90">
<attribute name="NAME" x="228.6" y="152.4" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="-Y" gate="G$1" x="233.68" y="147.32" smashed="yes" rot="MR90">
<attribute name="NAME" x="228.6" y="144.78" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="GND1" gate="1" x="200.66" y="124.46" smashed="yes">
<attribute name="VALUE" x="198.12" y="121.92" size="1.778" layer="96"/>
</instance>
<instance part="-Z" gate="G$1" x="259.08" y="127" smashed="yes" rot="MR90">
<attribute name="NAME" x="254" y="124.46" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="P+1" gate="VCC" x="144.78" y="195.58" smashed="yes">
<attribute name="VALUE" x="142.24" y="193.04" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="HOLE1" gate="G$1" x="180.34" y="96.52" smashed="yes"/>
<instance part="HOLE2" gate="G$1" x="180.34" y="88.9" smashed="yes"/>
<instance part="HOLE3" gate="G$1" x="180.34" y="81.28" smashed="yes"/>
<instance part="HOLE4" gate="G$1" x="180.34" y="73.66" smashed="yes"/>
<instance part="GND2" gate="1" x="170.18" y="66.04" smashed="yes">
<attribute name="VALUE" x="167.64" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="SW1" gate="G$1" x="226.06" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="227.965" y="194.31" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.52" y="196.85" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SW2" gate="G$1" x="226.06" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="227.965" y="179.07" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.52" y="181.61" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SW3" gate="G$1" x="269.24" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="271.145" y="194.31" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="266.7" y="196.85" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SW4" gate="G$1" x="269.24" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="271.145" y="179.07" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="266.7" y="181.61" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SW5" gate="G$1" x="157.48" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="159.385" y="179.07" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="154.94" y="181.61" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="DEP_SW1" class="0">
<segment>
<wire x1="231.14" y1="203.2" x2="233.68" y2="203.2" width="0.1524" layer="91"/>
<label x="233.68" y="203.2" size="1.778" layer="95" xref="yes"/>
<label x="233.68" y="203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="SW1" gate="G$1" pin="P"/>
</segment>
</net>
<net name="DEP_SW2" class="0">
<segment>
<wire x1="231.14" y1="187.96" x2="233.68" y2="187.96" width="0.1524" layer="91"/>
<label x="233.68" y="187.96" size="1.778" layer="95" xref="yes"/>
<pinref part="SW2" gate="G$1" pin="P"/>
</segment>
</net>
<net name="DEP_SW3" class="0">
<segment>
<wire x1="274.32" y1="203.2" x2="276.86" y2="203.2" width="0.1524" layer="91"/>
<label x="276.86" y="203.2" size="1.778" layer="95" xref="yes"/>
<pinref part="SW3" gate="G$1" pin="P"/>
</segment>
</net>
<net name="DEP_SW4" class="0">
<segment>
<wire x1="274.32" y1="187.96" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
<label x="276.86" y="187.96" size="1.778" layer="95" xref="yes"/>
<pinref part="SW4" gate="G$1" pin="P"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="264.16" y1="200.66" x2="259.08" y2="200.66" width="0.1524" layer="91"/>
<wire x1="259.08" y1="200.66" x2="259.08" y2="185.42" width="0.1524" layer="91"/>
<wire x1="264.16" y1="185.42" x2="259.08" y2="185.42" width="0.1524" layer="91"/>
<wire x1="259.08" y1="185.42" x2="259.08" y2="175.26" width="0.1524" layer="91"/>
<junction x="259.08" y="185.42"/>
<wire x1="220.98" y1="200.66" x2="215.9" y2="200.66" width="0.1524" layer="91"/>
<wire x1="215.9" y1="200.66" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
<wire x1="220.98" y1="185.42" x2="215.9" y2="185.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="185.42" x2="215.9" y2="175.26" width="0.1524" layer="91"/>
<junction x="215.9" y="185.42"/>
<wire x1="259.08" y1="175.26" x2="215.9" y2="175.26" width="0.1524" layer="91"/>
<wire x1="215.9" y1="175.26" x2="215.9" y2="172.72" width="0.1524" layer="91"/>
<junction x="215.9" y="175.26"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="O"/>
<pinref part="SW2" gate="G$1" pin="O"/>
<pinref part="SW3" gate="G$1" pin="O"/>
<pinref part="SW4" gate="G$1" pin="O"/>
</segment>
<segment>
<pinref part="STB1" gate="P2" pin="PIN"/>
<wire x1="172.72" y1="185.42" x2="165.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="165.1" y1="185.42" x2="165.1" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<junction x="165.1" y="177.8"/>
</segment>
<segment>
<pinref part="3V3_1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<wire x1="93.98" y1="124.46" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
<pinref part="3V3_2" gate="G$1" pin="1"/>
<wire x1="93.98" y1="116.84" x2="96.52" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="93.98" y1="116.84" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<junction x="93.98" y="116.84"/>
<wire x1="93.98" y1="111.76" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
<junction x="93.98" y="111.76"/>
<pinref part="UNREG1" gate="G$1" pin="1"/>
<wire x1="119.38" y1="139.7" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<wire x1="116.84" y1="139.7" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<pinref part="UNREG2" gate="G$1" pin="1"/>
<wire x1="116.84" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<wire x1="116.84" y1="111.76" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<junction x="116.84" y="132.08"/>
<wire x1="116.84" y1="111.76" x2="139.7" y2="111.76" width="0.1524" layer="91"/>
<junction x="116.84" y="111.76"/>
<pinref part="5V" gate="G$1" pin="1"/>
<wire x1="142.24" y1="154.94" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="111.76" x2="139.7" y2="147.32" width="0.1524" layer="91"/>
<pinref part="RAW_PWR" gate="G$1" pin="1"/>
<wire x1="139.7" y1="147.32" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="147.32" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
<junction x="139.7" y="147.32"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="96.52" y1="177.8" x2="96.52" y2="182.88" width="0.1524" layer="91"/>
<junction x="96.52" y="177.8"/>
</segment>
<segment>
<pinref part="+X" gate="G$1" pin="1"/>
<wire x1="203.2" y1="139.7" x2="200.66" y2="139.7" width="0.1524" layer="91"/>
<wire x1="200.66" y1="139.7" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<pinref part="-X" gate="G$1" pin="1"/>
<wire x1="200.66" y1="132.08" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="200.66" y1="132.08" x2="200.66" y2="127" width="0.1524" layer="91"/>
<junction x="200.66" y="132.08"/>
<wire x1="200.66" y1="127" x2="223.52" y2="127" width="0.1524" layer="91"/>
<junction x="200.66" y="127"/>
<pinref part="+Y" gate="G$1" pin="1"/>
<wire x1="226.06" y1="154.94" x2="223.52" y2="154.94" width="0.1524" layer="91"/>
<wire x1="223.52" y1="154.94" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<pinref part="-Y" gate="G$1" pin="1"/>
<wire x1="223.52" y1="147.32" x2="226.06" y2="147.32" width="0.1524" layer="91"/>
<wire x1="223.52" y1="127" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<junction x="223.52" y="147.32"/>
<wire x1="223.52" y1="127" x2="251.46" y2="127" width="0.1524" layer="91"/>
<junction x="223.52" y="127"/>
<pinref part="-Z" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="HOLE1" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="170.18" y1="96.52" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<pinref part="HOLE2" gate="G$1" pin="P$1"/>
<wire x1="170.18" y1="88.9" x2="175.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="170.18" y1="88.9" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
<junction x="170.18" y="88.9"/>
<pinref part="HOLE3" gate="G$1" pin="P$1"/>
<wire x1="170.18" y1="81.28" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="81.28" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<junction x="170.18" y="81.28"/>
<pinref part="HOLE4" gate="G$1" pin="P$1"/>
<wire x1="170.18" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<junction x="170.18" y="73.66"/>
<pinref part="GND2" gate="1" pin="GND"/>
<junction x="170.18" y="68.58"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="5V" gate="G$1" pin="2"/>
<wire x1="157.48" y1="154.94" x2="162.56" y2="154.94" width="0.1524" layer="91"/>
<label x="162.56" y="154.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="UNREG1" class="0">
<segment>
<pinref part="UNREG1" gate="G$1" pin="2"/>
<wire x1="134.62" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
<label x="142.24" y="139.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V3_2" class="0">
<segment>
<pinref part="3V3_2" gate="G$1" pin="2"/>
<wire x1="111.76" y1="116.84" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
<label x="119.38" y="116.84" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="RAW_PWR" class="0">
<segment>
<pinref part="RAW_PWR" gate="G$1" pin="2"/>
<wire x1="157.48" y1="147.32" x2="162.56" y2="147.32" width="0.1524" layer="91"/>
<label x="162.56" y="147.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="114.3" y1="195.58" x2="119.38" y2="195.58" width="0.1524" layer="91"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="119.38" y1="195.58" x2="119.38" y2="203.2" width="0.1524" layer="91"/>
<label x="119.38" y="187.96" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="144.78" y1="193.04" x2="144.78" y2="185.42" width="0.1524" layer="91"/>
<wire x1="144.78" y1="185.42" x2="152.4" y2="185.42" width="0.1524" layer="91"/>
<label x="139.7" y="182.88" size="1.778" layer="95"/>
<pinref part="SW5" gate="G$1" pin="O"/>
</segment>
</net>
<net name="UNREG2" class="0">
<segment>
<pinref part="UNREG2" gate="G$1" pin="2"/>
<wire x1="134.62" y1="132.08" x2="142.24" y2="132.08" width="0.1524" layer="91"/>
<label x="142.24" y="132.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V3_1" class="0">
<segment>
<pinref part="3V3_1" gate="G$1" pin="2"/>
<wire x1="111.76" y1="124.46" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<label x="119.38" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="STB1" gate="P1" pin="PIN"/>
<wire x1="172.72" y1="187.96" x2="162.56" y2="187.96" width="0.1524" layer="91"/>
<pinref part="SW5" gate="G$1" pin="P"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="104.14" y1="195.58" x2="96.52" y2="195.58" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="96.52" y1="195.58" x2="96.52" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWRSC_+X" class="0">
<segment>
<pinref part="+X" gate="G$1" pin="2"/>
<wire x1="218.44" y1="139.7" x2="226.06" y2="139.7" width="0.1524" layer="91"/>
<label x="226.06" y="139.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWRSC_-X" class="0">
<segment>
<pinref part="-X" gate="G$1" pin="2"/>
<wire x1="218.44" y1="132.08" x2="226.06" y2="132.08" width="0.1524" layer="91"/>
<label x="226.06" y="132.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWRSC_-Y" class="0">
<segment>
<pinref part="-Y" gate="G$1" pin="2"/>
<wire x1="241.3" y1="147.32" x2="248.92" y2="147.32" width="0.1524" layer="91"/>
<label x="248.92" y="147.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MTQ_PWR_-Z" class="0">
<segment>
<pinref part="CON3" gate="P1" pin="PIN"/>
<wire x1="233.68" y1="91.44" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<label x="226.06" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MTQ_RTN_-Z" class="0">
<segment>
<pinref part="CON3" gate="P2" pin="PIN"/>
<wire x1="233.68" y1="88.9" x2="226.06" y2="88.9" width="0.1524" layer="91"/>
<label x="226.06" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWRSC_+Y" class="0">
<segment>
<pinref part="+Y" gate="G$1" pin="2"/>
<wire x1="241.3" y1="154.94" x2="248.92" y2="154.94" width="0.1524" layer="91"/>
<label x="248.92" y="154.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWRSC_-Z" class="0">
<segment>
<pinref part="-Z" gate="G$1" pin="2"/>
<wire x1="266.7" y1="127" x2="271.78" y2="127" width="0.1524" layer="91"/>
<label x="271.78" y="127" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MTQ_PWR_-X" class="0">
<segment>
<pinref part="CON1" gate="P1" pin="PIN"/>
<wire x1="233.68" y1="106.68" x2="226.06" y2="106.68" width="0.1524" layer="91"/>
<label x="226.06" y="106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MTQ_RTN_-X" class="0">
<segment>
<pinref part="CON1" gate="P2" pin="PIN"/>
<wire x1="233.68" y1="104.14" x2="226.06" y2="104.14" width="0.1524" layer="91"/>
<label x="226.06" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MTQ_PWR_-Y" class="0">
<segment>
<pinref part="CON2" gate="P1" pin="PIN"/>
<wire x1="233.68" y1="99.06" x2="226.06" y2="99.06" width="0.1524" layer="91"/>
<label x="226.06" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MTQ_RTN_-Y" class="0">
<segment>
<pinref part="CON2" gate="P2" pin="PIN"/>
<wire x1="233.68" y1="96.52" x2="226.06" y2="96.52" width="0.1524" layer="91"/>
<label x="226.06" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
