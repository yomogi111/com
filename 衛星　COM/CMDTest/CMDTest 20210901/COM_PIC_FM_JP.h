/* Thiaaaaaas file contains everything related to compic functions and compic settings
   ,compic comuniction ports
*/

//_______________________________________________________________________________________________________________
#use rs232(baud=9600, parity=N, xmit=PIN_B6, rcv=PIN_B7, bits=8, stream=PORT1)    //PC reading port
//_______________________________________________________________________________________________________________
#use rs232(uart1,baud=115200, parity=N, bits=8, stream=TR_CP)                      //UART________TRX 

#use rs232(baud=4800, parity=N, xmit=PIN_B3, rcv=PIN_B4, bits=8, stream=RP_CP, ERRORS)    //UART________RESET-PIC
#use rs232(baud=9600, parity=N, xmit=PIN_B1, rcv=PIN_B2, bits=8, stream=MP_CP, ERRORS)    //UART________MAIN-PIC   


