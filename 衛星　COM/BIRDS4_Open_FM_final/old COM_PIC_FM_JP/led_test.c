#include <16F1789.h>
#fuses HS,NOWDT,NOPROTECT,PUT,BROWNOUT,NOLVP

#use delay(CLOCK = 16000000)
#use rs232(BAUD =9600,XMIT = PIN_B6,RCV = PIN_B7)

void main()
{
   while(1)
   {
      printf("Hello!!\r\n");
      delay_ms(500);
   }
}
