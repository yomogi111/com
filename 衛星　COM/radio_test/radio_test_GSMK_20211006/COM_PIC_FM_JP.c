#include <16F1789.h>
BYTE next_in = 0;
int t;
#device adc=16

#use delay(crystal=16mhz)
#include <COMPICREG.h>                                                           // com pic registers header
#include <COM_PIC_FM_JP.h>                                                     // com main header file
//#fuses HS,NOPROtECt,NOWDT,PUT,BROWNOUT,NOLVP                                                 // com main header file

int auto_flag1 = 1;
int counti = 0;

#define CHECK_CMD (cr==pk) && ((sat_id == 0xAA)||(sat_id == 0xBB)||(sat_id == 0x59))               // (cr == pk) checking crc, AA is sat id
                                                                                 // BB is the common id for all sat

#int_rda
void serial_isr()
{
   buffer[next_in]=fgetc(tr_cp);
   t=next_in;
   //next_in=(next_in+1)% 17 ;
   next_in=(next_in+1)% 15 ;
   //fprintf(PORT1,"buffer[%d] = %x\n\r",t,buffer[t]);
} 
void buffer_check()
{
   if(buffer[12] != 0)
   {
      for(int i = 0;i <= 13; i++)
      {
      fprintf(PORT1,"buffer[%d] = %x\n\r",i,buffer[i]);
      }
   }
   else if(buffer[13] != 0)
   {
      for(int i = 0;i <= 13; i++)
      {
      fprintf(PORT1,"buffer[%d] = %x\n\r",i,buffer[i]);
      }
   }
   else if(buffer[14] != 0)
   {
      for(int i = 0;i <= 13; i++)
      {
      fprintf(PORT1,"buffer[%d] = %x\n\r",i,buffer[i]);
      }
   }
   
}
   
void main(void)
{
fprintf(PORT1,"GS integration test1 start \n\r");
settings();                                                                      // Every initial settings belongs to com pic is included in this function
Setup_wdt(wdt_256s);                                                             // Wdt setup
fprintf(PORT1,"end_Setup_wdt\n\r");
unsigned int32 start_address = make32(cmd_Pckt[4],cmd_Pckt[5],cmd_Pckt[6],cmd_Pckt[7]);
unsigned int8 dsize[4];
unsigned int32 data_size;
char cmd;
//unsigned int32 pckt_num     = 100;
//delay_ms(5000);                                                                  // Just delay before start
/*delay_ms(30000);
delay_ms(30000);*/

RX_ON();                                                                         // Reception mode activated
fprintf(PORT1,"Enter the key\n\r");
    While(true)
    { 
    //if(kbhit(PORT1))
    //{
      //fprintf(PORT1,"Enter the key\n\r");
      cmd = fgetc(PORT1);
      switch(cmd)
      {
         case 't':       
            TX_ON();
            fprintf(PORT1,"TX_ON\n\r");
            for(unsigned int32 i = 0; i < 4; i++)
            {
               fprintf(PORT1,"i = %x\n\r",i);
               //fprintf(PORT1,"start_address + 1 = %lx\n\r",start_address + i);
               dsize[i] = SF_BYTE_READ(start_address + i);
               fprintf(PORT1,"dsize[%lx] = %lx\n\r",i,dsize[i]);
            }
            data_size = make32(dsize[3],dsize[2],dsize[1],dsize[0]);
            fprintf(PORT1,"data_size = %lx\n\r",data_size);
            Send_Data_Packets(start_address,data_size,0,0);
            fprintf(PORT1,"send_data_packets\n\r");
            //fprintf(PORT1,"delay_now\n\r");
            //delay_ms(5000);
            RX_ON();
            fprintf(PORT1,"RX_ON\n\r");
            break;
         case 'r':
            fprintf(PORT1,"start_receive_mode\n\r");
            for(int i = 0; i < 10 ;i++)
            {
               buffer_corection();
               
               
               
               
               delay_ms(300);
            } // for loop finish here
            break;

         case 'z':
            fprintf(PORT1,"start_receive and send_mode\n\r");
            for(int i = 0; i < 10 ;i++)
            {
               buffer_corection();
               if(CHECK_CMD)
                {
                 if(CMD_PCKT[2] == 0xAA) { Sync(); }                // 38              sync or delay
                       
                 PACKET_DOWNLOAD();                                 // 10 , 11
                 LENGTH_DOWNLOAD();                                 // 13 , 14
                 ONE_PCKT_DOWNLOAD();                               // 15 , 16
                 Data_down_from_SFM_with_access();                  // 35
                 SET_OF_PACKETS_DOWNLOAD_WITH_ACCESS();             // 44
                 
                 SET_OF_PACKETS_DOWNLOAD();                         // 40,41           40 00 00 00 00    00 00 setsize setnumber      
                 
                 SEND_COM_TEMP_RSSI_TO_GS();                        // 30              30 00 00 00 00    00 00 00 00 
                 FM_COPY_AND_DATA_DOWN_FROM_CFM();                  // 27              25 00 00 00 00    00 00 packet size
                 
                 
                 FM_COPY();                                         // 17  without getting accees
                 FM_COPY_WITH_ACCESS();                             // 22
                 GET_SFM_ACCESS_GIVEN_TIME();                       // 42
                 
                 CMD_TO_MPIC();                                     // A0
                 CW_SEND_BGSC();                                    // 21
                 CW_UPDATE_MISSION();                               // 20 
               }   
                
               next_in=0;
               RX_BFR_ZERO();
               
               
               delay_ms(300);
            } // for loop finish here
            break;
         default:
            printf(PORT1,"dont_use_key");
         
      }//switch finish here
    //}//if finish here
    }                                                                             //while loop finish here
}


