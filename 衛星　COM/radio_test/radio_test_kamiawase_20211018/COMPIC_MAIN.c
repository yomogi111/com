#include <16F1789.h>
BYTE next_in = 0;
int t;
#device adc=16
//#fuses NOMCLR,WDT,NOPUT,NOBROWNOUT
#use delay(crystal=16mhz)
#include <COMPICREG.h>              // com pic registers header
#include <COMPIC_MAIN_SRI.h>            // com main header file

int auto_flag1 = 1;
int counti = 0;

#define CHECK_CMD (cr==pk) && ((sat_id == 0xAA)||(sat_id == 0xBB))     // (cr == pk) checking crc, AA is SL sat id, 
                                                                       // BB is the common id for all sat

#int_rda
void serial_isr()
{
   buffer[next_in]=fgetc(tr_cp);
   t=next_in;
   next_in=(next_in+1)% 17 ;   
} 

void CW_YOMOGI()
{
CW_Y();
CW_O();
CW_M();
CW_O();
CW_G();
CW_I();
}

void main(void)
{
fprintf(PORT1,"start_to_main\n\r");
settings();                    // Every initial settings belongs to com pic is included in this function
fprintf(PORT1,"start_to_Setup_wdt\n\r");
Setup_wdt(wdt_256s);
fprintf(PORT1,"end_to_Setup_wdt\n\r");

//delay_ms(5000);                  // Just delay before start
//delay_ms(30000);                  // Just delay before start

RX_ON();
fprintf(PORT1,"RX_ON\n\r");
    While(true)
    {  
       if(fgetc(PORT1))
       {
       fprintf(PORT1,"key_hited\n\r");
       CW_ON();
       fprintf(PORT1,"CW_ON\n\r");
       CW_O();
       CW_O();
       //CW_YOMOGI();
       fprintf(PORT1,"send_cw\n\r");
       }
      

      
   } //while loop finish here
}
