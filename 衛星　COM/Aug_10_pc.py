import serial
from PIL import Image
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True
import io
import time

ser = serial.Serial("COM10", 115200)

byte_data_full = b''
n = 0

while True:
    byte_data = ser.read_all()

    if 0 < len(byte_data):
        byte_data_full += byte_data
        n = 1

    else:
        if n == 1:
            n = 3

    if n == 3:
        break
    time.sleep(1)

packet_len = len(byte_data_full)
image_data = b''
i = 0
image = False
image_count = 0
image_num = list()
max_image_num = 0

crc_Reg = 0x0001
calc = 0x8404
data_num = 30
cal_data = list()

CRC_num = list()
CRC_Result = list()
image_data_store = list()

while True:
    if byte_data_full[i:i + 3] == b'CRC':
        CRC_num.append(byte_data_full[i + 3:i + 6])
        i += 6
    elif byte_data_full[i:i + 5] == b'start':
        image_data += byte_data_full[i + 7:i + 8]
        max_image_num = byte_data_full[i + 5]
        image_num.append(byte_data_full[i + 6])
        image = True
        data = byte_data_full[i + 7:i + 8]
        i += 8
    elif byte_data_full[i:i + 3] == b'end':
        image = False

        image_data_store.append(data)
        crc_Reg = 0x0001
        calc = 0x8404
        data_num = 30
        cal_data = list()
        for x in range(len(data)):
            cal_data.append(data[x])

            for n in range(8):
                w = (crc_Reg ^ cal_data[x]) & 0x0001
                crc_Reg = crc_Reg >> 1

                if w == 1:
                    crc_Reg = crc_Reg ^ calc

                cal_data[x] = cal_data[x] >> 1

        crc_Reg = crc_Reg ^ 0xFFFF
        crc_Reg2 = crc_Reg >> 8
        CRC_Result.append(crc_Reg.to_bytes(2, "big") + crc_Reg2.to_bytes(1, "big"))

        i += 3
    elif image:
        image_data += byte_data_full[i:i + 1]
        data += byte_data_full[i:i + 1]
        i += 1
    else:
        i += 1
    if i == packet_len:
        break

count = 0

for i in range(len(CRC_Result)):
    if CRC_Result[i] == CRC_Result[i]:
        count += 1

if count == len(CRC_Result):
    image_enter = Image.open(io.BytesIO(image_data))
    image_enter.show()

# store_image_num = list()

# for n in range(len(image_num)):
#     if (image_num[n] != int(str(n)[-2::])):
#         print("E")
#         store_image_num.append(n)

# print(store_image_num)
# if len(store_image_num) == 0:


# if(int(str(len(image_num))[-2::]) == max_image_num):
#     image_enter = Image.open(io.BytesIO(image_data))
#     image_enter.show()
