#include <16F1789.h>
BYTE next_in = 0;
int t;

#device adc=16
//#fuses NOMCLR,WDT,NOPUT,NOBROWNOUT


#use delay(oscillator=16mhz)
#include <COMPICREG.h>                                                           // com pic registers header
#include <COM_PIC_FM_JP.h>                                                     // com main header file
#fuses HS,NOWDT,NOPROTECT,PUT,BROWNOUT,NOLVP


int auto_flag1 = 1;
int counti = 0;

#define CHECK_CMD (cr==pk) && ((sat_id == 0xAA)||(sat_id == 0xBB))               // (cr == pk) checking crc, AA is sat id
                                                                                 // BB is the common id for all sat

#int_rda
void serial_isr()
{
   buffer[next_in]=fgetc(tr_cp);
   t=next_in;
   next_in=(next_in+1)% 17 ;   
} 


void sendCW_answer( int result)
{
   switch(result)
   {
   case 1:
    CW_1();
     break;
   case 2:
    CW_2();
     break;
   case 3:
    CW_3();
     break;
   case 4:
    CW_4();
     break;
   case 5:
    CW_5();
     break;
   case 6:
    CW_6();
     break;
   case 7:
    CW_7();
     break;
   case 8:
    CW_8();
     break;
   case 9:
    CW_9();
     break;
   case 10:
    CW_1();CW_0();
     break;
   }
}

void main(void)
{
int cmd,a;

fputs("hello \r\n", TR_CP);
fprintf(TR_CP, "world\r\n");
fputc('d', TR_CP);
 cmd = getc();
 a = cmd - '0';
 a = a + 1;
 fprintf(TR_CP,"kotae ha %d \r\n",a);
fputc(0x31, TR_CP);
settings();                                                                      // Every initial settings belongs to com pic is included in this function
//Setup_wdt(wdt_256s);                                                             // Wdt setup

delay_ms(5000);                                                                  // Just delay before start
/*delay_ms(30000);
delay_ms(30000);*/

fputc('b', TR_CP);

RX_ON();                                                                         // Reception mode activated

fputc('c', TR_CP);
    While(true)
    { 
      

      
      //______________(1)______CW_RELATED_TASKS__________________________________________<<
  
       CW_PCKT();
      // restart_wdt();  
      delay_ms(1000);
   fprintf(TR_CP,"start answer\r\n");
         sendCW_answer(a) ;
   fputc('d', TR_CP);
                                                                        // for loop finish here

    
   }                                                                             //while loop finish here
}


